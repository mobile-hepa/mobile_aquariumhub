import 'package:aquariumhub/model/remote/requests/SignInRequest.dart';
import 'package:aquariumhub/model/remote/response/SignInDataResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/SignInDataRepository.dart';
import 'package:rxdart/rxdart.dart';

class SignInDataBloc {
  final SignInDataRepository _signInDataRepository = SignInDataRepository();

  final BehaviorSubject<ApiResponse<SignInDataResponse>> _subject =
      BehaviorSubject<ApiResponse<SignInDataResponse>>();

  executeSignInData(SignInRequest SignInRequest) {
    _signInDataRepository.executeSignInData(SignInRequest, _subject);
  }

  BehaviorSubject<ApiResponse<SignInDataResponse>> get subject => _subject;

  /// functions that used to  close the Subject stream
  disposeSignInDataSubject() {
    _subject.close();
  }
}
