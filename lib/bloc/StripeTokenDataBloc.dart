import 'package:aquariumhub/model/remote/requests/StripeTokenRequest.dart';
import 'package:aquariumhub/model/remote/response/StripeTokenResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/StripeTokenDataRepository.dart';
import 'package:rxdart/rxdart.dart';

class StripeTokenDataBloc {
  final StripeTokenDataRepository _stripeTokenDataRepository =
      StripeTokenDataRepository();

  final BehaviorSubject<ApiResponse<StripeTokenResponse>> _subject =
      BehaviorSubject<ApiResponse<StripeTokenResponse>>();

  executeStripeTokenData(StripeTokenRequest stripeTokenRequest) {
    _stripeTokenDataRepository.executeStripeTokenData(
        stripeTokenRequest, _subject);
  }

  BehaviorSubject<ApiResponse<StripeTokenResponse>> get subject => _subject;

  /// functions that used to  close the Subject stream
  disposeStripeTokenDataSubject() {
    _subject.close();
  }
}
