import 'package:aquariumhub/model/remote/response/SignOutResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/SignOutRepository.dart';
import 'package:rxdart/rxdart.dart';

class SignOutBloc {
  final SignOutRepository _signOutRepository = SignOutRepository();

  final BehaviorSubject<ApiResponse<SignOutResponse>> _subject =
      BehaviorSubject<ApiResponse<SignOutResponse>>();

  executeSignOutData() {
    _signOutRepository.executeSignOut(_subject);
  }

  BehaviorSubject<ApiResponse<SignOutResponse>> get subject => _subject;

  /// functions that used to  close the Subject stream
  disposeSignOutSubject() {
    _subject.close();
  }
}
