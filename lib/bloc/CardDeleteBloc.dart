import 'package:aquariumhub/model/remote/requests/CardDeleteRequest.dart';
import 'package:aquariumhub/model/remote/response/CardDeleteResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/CardDeleteRepository.dart';
import 'package:rxdart/rxdart.dart';

class CardDeleteBloc {
  final CardDeleteRepository _cardDeleteRepository = CardDeleteRepository();

  final BehaviorSubject<ApiResponse<CardDeleteResponse>> _subject =
      BehaviorSubject<ApiResponse<CardDeleteResponse>>();

  executeCardDelete(CardDeleteRequest cardListRequest) {
    _cardDeleteRepository.executeCardDelete(cardListRequest, _subject);
  }

  BehaviorSubject<ApiResponse<CardDeleteResponse>> get subject => _subject;

  /// functions that used to  close the Subject stream
  disposeCardDeleteSubject() {
    _subject.close();
  }
}
