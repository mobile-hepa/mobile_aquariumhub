import 'package:aquariumhub/model/remote/requests/ShopListRequest.dart';
import 'package:aquariumhub/model/remote/response/ShopListDataResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/ShopListDataRepository.dart';
import 'package:rxdart/rxdart.dart';

class ShopListDataBloc {
  final ShopListDataRepository _shopListDataRepository =
      ShopListDataRepository();

  final BehaviorSubject<ApiResponse<ShopListDataResponse>> _subject =
      BehaviorSubject<ApiResponse<ShopListDataResponse>>();

  executeShopListDataRepository(ShopListRequest ShopListRequest) {
    _shopListDataRepository.executeShopListDataRepository(
        ShopListRequest, _subject);
  }

  BehaviorSubject<ApiResponse<ShopListDataResponse>> get subject => _subject;

  /// functions that used to  close the Subject stream
  disposeShopListDataSubject() {
    _subject.close();
  }
}
