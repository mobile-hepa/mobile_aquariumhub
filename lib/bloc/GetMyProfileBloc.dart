import 'package:aquariumhub/model/remote/response/GetMyProfileResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/GetMyProfileRepository.dart';
import 'package:rxdart/rxdart.dart';

class GetMyProfileBloc {
  final GetMyProfileRepository _getMyProfileRepository =
      GetMyProfileRepository();

  final BehaviorSubject<ApiResponse<GetMyProfileResponse>> _subject =
      BehaviorSubject<ApiResponse<GetMyProfileResponse>>();

  executeGetMyProfile() {
    _getMyProfileRepository.executeGetMyProfile(_subject);
  }

  BehaviorSubject<ApiResponse<GetMyProfileResponse>> get subject => _subject;

  /// functions that used to  close the Subject stream
  disposeGetMyProfileSubject() {
    _subject.close();
  }
}
