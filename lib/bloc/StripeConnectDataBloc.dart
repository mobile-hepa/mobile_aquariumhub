import 'package:aquariumhub/model/remote/requests/StripeConnectRequest.dart';
import 'package:aquariumhub/model/remote/response/StripeConnectResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/StripeConnectDataRepository.dart';
import 'package:rxdart/rxdart.dart';

class StripeConnectDataBloc {
  final StripeConnectDataRepository _StripeConnectDataRepository =
      StripeConnectDataRepository();

  final BehaviorSubject<ApiResponse<StripeConnectResponse>> _subject =
      BehaviorSubject<ApiResponse<StripeConnectResponse>>();

  executeStripeTokenData(StripeConnectRequest StripeConnectRequest) {
    _StripeConnectDataRepository.executeStripeConnectData(
        StripeConnectRequest, _subject);
  }

  BehaviorSubject<ApiResponse<StripeConnectResponse>> get subject => _subject;

  /// functions that used to  close the Subject stream
  disposeStripeTokenDataSubject() {
    _subject.close();
  }
}
