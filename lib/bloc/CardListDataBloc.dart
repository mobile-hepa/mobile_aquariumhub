import 'package:aquariumhub/model/remote/requests/CardListRequest.dart';
import 'package:aquariumhub/model/remote/response/CardListResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/CardListDataRepository.dart';
import 'package:rxdart/rxdart.dart';

class CardListDataBloc {
  final CardListDataRepository _cardListDataRepository =
      CardListDataRepository();

  final BehaviorSubject<ApiResponse<CardListResponse>> _subject =
      BehaviorSubject<ApiResponse<CardListResponse>>();

  executeCardListData(CardListRequest CardListRequest) {
    _cardListDataRepository.executeCardListData(CardListRequest, _subject);
  }

  BehaviorSubject<ApiResponse<CardListResponse>> get subject => _subject;

  /// functions that used to  close the Subject stream
  disposeStripeTokenDataSubject() {
    _subject.close();
  }
}
