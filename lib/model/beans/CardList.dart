class CardList {
  bool success;
  CardListData data;

  CardList({this.success, this.data});

  CardList.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data =
        json['data'] != null ? new CardListData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class CardListData {
  Stripe stripe;

  CardListData({this.stripe});

  CardListData.fromJson(Map<String, dynamic> json) {
    stripe =
        json['stripe'] != null ? new Stripe.fromJson(json['stripe']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.stripe != null) {
      data['stripe'] = this.stripe.toJson();
    }
    return data;
  }
}

class Stripe {
  String description;
  Value value;
  int userId;
  String pk;
  String pkAltern;
  List<StripeCards> stripeCards;
  List<StripeAlternCards> stripeAlternCards;

  Stripe(
      {this.description,
      this.value,
      this.userId,
      this.pk,
      this.pkAltern,
      this.stripeCards,
      this.stripeAlternCards});

  Stripe.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    value = json['value'] != null ? new Value.fromJson(json['value']) : null;
    userId = json['user_id'];
    pk = json['pk'];
    pkAltern = json['pk_altern'];
    if (json['stripe_cards'] != null) {
      stripeCards = new List<StripeCards>();
      json['stripe_cards'].forEach((v) {
        stripeCards.add(new StripeCards.fromJson(v));
      });
    }
    if (json['stripe_altern_cards'] != null) {
      stripeAlternCards = new List<StripeAlternCards>();
      json['stripe_altern_cards'].forEach((v) {
        stripeAlternCards.add(new StripeAlternCards.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    if (this.value != null) {
      data['value'] = this.value.toJson();
    }
    data['user_id'] = this.userId;
    data['pk'] = this.pk;
    data['pk_altern'] = this.pkAltern;
    if (this.stripeCards != null) {
      data['stripe_cards'] = this.stripeCards.map((v) => v.toJson()).toList();
    }
    if (this.stripeAlternCards != null) {
      data['stripe_altern_cards'] =
          this.stripeAlternCards.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Value {
  String id;
  String title;

  Value({this.id, this.title});

  Value.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    return data;
  }
}

class StripeCards {
  String last4;
  String cardType;
  String expiryYear;
  String expiryMonth;
  String tokenId;
  String gatewayId;
  String token;
  String userId;
  String type;
  String isDefault;

  StripeCards(
      {this.last4,
      this.cardType,
      this.expiryYear,
      this.expiryMonth,
      this.tokenId,
      this.gatewayId,
      this.token,
      this.userId,
      this.type,
      this.isDefault});

  StripeCards.fromJson(Map<String, dynamic> json) {
    last4 = json['last4'];
    cardType = json['card_type'];
    expiryYear = json['expiry_year'];
    expiryMonth = json['expiry_month'];
    tokenId = json['token_id'];
    gatewayId = json['gateway_id'];
    token = json['token'];
    userId = json['user_id'];
    type = json['type'];
    isDefault = json['is_default'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['last4'] = this.last4;
    data['card_type'] = this.cardType;
    data['expiry_year'] = this.expiryYear;
    data['expiry_month'] = this.expiryMonth;
    data['token_id'] = this.tokenId;
    data['gateway_id'] = this.gatewayId;
    data['token'] = this.token;
    data['user_id'] = this.userId;
    data['type'] = this.type;
    data['is_default'] = this.isDefault;
    return data;
  }
}

class StripeAlternCards {
  String last4;
  String cardType;
  String expiryYear;
  String expiryMonth;
  String tokenId;
  String gatewayId;
  String token;
  String userId;
  String type;
  String isDefault;

  StripeAlternCards(
      {this.last4,
      this.cardType,
      this.expiryYear,
      this.expiryMonth,
      this.tokenId,
      this.gatewayId,
      this.token,
      this.userId,
      this.type,
      this.isDefault});

  StripeAlternCards.fromJson(Map<String, dynamic> json) {
    last4 = json['last4'];
    cardType = json['card_type'];
    expiryYear = json['expiry_year'];
    expiryMonth = json['expiry_month'];
    tokenId = json['token_id'];
    gatewayId = json['gateway_id'];
    token = json['token'];
    userId = json['user_id'];
    type = json['type'];
    isDefault = json['is_default'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['last4'] = this.last4;
    data['card_type'] = this.cardType;
    data['expiry_year'] = this.expiryYear;
    data['expiry_month'] = this.expiryMonth;
    data['token_id'] = this.tokenId;
    data['gateway_id'] = this.gatewayId;
    data['token'] = this.token;
    data['user_id'] = this.userId;
    data['type'] = this.type;
    data['is_default'] = this.isDefault;
    return data;
  }
}
