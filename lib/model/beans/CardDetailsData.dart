class CardDetailsData {
  bool success;
  CardData data;

  CardDetailsData({this.success, this.data});

  CardDetailsData.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? new CardData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class CardData {
  String tokenId;
  int expiryMonth;
  int expiryYear;
  String last4;
  String cardType;
  String tokenAlternId;
  String html;
  String info;

  CardData({
    this.tokenId,
    this.expiryMonth,
    this.expiryYear,
    this.last4,
    this.cardType,
    this.tokenAlternId,
    this.html,
    this.info,
  });

  CardData.fromJson(Map<String, dynamic> json) {
    tokenId = json['token_id'];
    expiryMonth = json['expiry_month'];
    expiryYear = json['expiry_year'];
    last4 = json['last4'];
    cardType = json['card_type'];
    tokenAlternId = json['token_altern_id'];
    html = json['html'];
    info = json['info'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token_id'] = this.tokenId;
    data['expiry_month'] = this.expiryMonth;
    data['expiry_year'] = this.expiryYear;
    data['last4'] = this.last4;
    data['card_type'] = this.cardType;
    data['token_altern_id'] = this.tokenAlternId;
    data['html'] = this.html;
    data['info'] = this.info;
    return data;
  }
}
