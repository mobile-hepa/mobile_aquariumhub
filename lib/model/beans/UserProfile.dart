class UserProfile {
  bool success;
  UserProfileData data;

  UserProfile({this.success, this.data});

  UserProfile.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null
        ? new UserProfileData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class UserProfileData {
  String following;
  String followers;
  String products;
  bool stripeConnected;
  bool sellerEnabled;
  String storeName;
  String phone;
  Address address;
  int bannerId;
  int gravatarId;
  String bannerUrl;
  String gravatarUrl;
  int myId;

  UserProfileData(
      {this.following,
      this.followers,
      this.products,
      this.stripeConnected,
      this.sellerEnabled,
      this.storeName,
      this.phone,
      this.address,
      this.bannerId,
      this.gravatarId,
      this.bannerUrl,
      this.gravatarUrl,
      this.myId});

  UserProfileData.fromJson(Map<String, dynamic> json) {
    following = json['following'];
    followers = json['followers'];
    products = json['products'];
    stripeConnected = json['stripe_connected'];
    sellerEnabled = json['seller_enabled'];
    storeName = json['store_name'];
    phone = json['phone'];
    address =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
    bannerId = json['banner_id'];
    gravatarId = json['gravatar_id'];
    bannerUrl = json['banner_url'];
    gravatarUrl = json['gravatar_url'];
    myId = json['my_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['following'] = this.following;
    data['followers'] = this.followers;
    data['products'] = this.products;
    data['stripe_connected'] = this.stripeConnected;
    data['seller_enabled'] = this.sellerEnabled;
    data['store_name'] = this.storeName;
    data['phone'] = this.phone;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    data['banner_id'] = this.bannerId;
    data['gravatar_id'] = this.gravatarId;
    data['banner_url'] = this.bannerUrl;
    data['gravatar_url'] = this.gravatarUrl;
    data['my_id'] = this.myId;
    return data;
  }
}

class Address {
  String street1;
  String street2;
  String city;
  String zip;
  String country;
  String state;

  Address(
      {this.street1,
      this.street2,
      this.city,
      this.zip,
      this.country,
      this.state});

  Address.fromJson(Map<String, dynamic> json) {
    street1 = json['street_1'];
    street2 = json['street_2'];
    city = json['city'];
    zip = json['zip'];
    country = json['country'];
    state = json['state'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['street_1'] = this.street1;
    data['street_2'] = this.street2;
    data['city'] = this.city;
    data['zip'] = this.zip;
    data['country'] = this.country;
    data['state'] = this.state;
    return data;
  }
}
