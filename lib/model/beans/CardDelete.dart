class CardDelete {
  bool success;
  CardDeleteData data;

  CardDelete({this.success, this.data});

  CardDelete.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data =
        json['data'] != null ? new CardDeleteData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class CardDeleteData {
  String html;

  CardDeleteData({this.html});

  CardDeleteData.fromJson(Map<String, dynamic> json) {
    html = json['html'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['html'] = this.html;
    return data;
  }
}
