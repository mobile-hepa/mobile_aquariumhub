class ShopData {
  List<ShopListData> data;

  ShopData({this.data});

  ShopData.fromJson(List<dynamic> json) {
    if (json != null) {
      data = new List<ShopListData>();
      json.forEach((v) {
        data.add(new ShopListData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data[''] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ShopListData {
  int id;
  String name;
  String slug;
  String permalink;
  String type;
  String status;
  String description;
  String shortDescription;
  String price;
  String regularPrice;
  String salePrice;
  int stockQuantity;
  String stockStatus;
  List<Categories> categories;
  List<Images> images;
  int vendor;
  String storeName;
  Store store;
  String sellerId;
  String sellerName;
  String sellerAvatar;
  List<String> colors;
  List<String> sizes;
  String likes;
  String shippable;
  String localPickup;
  String bestOffer;
  String comments;
  bool liked;
  Links lLinks;

  ShopListData(
      {this.id,
      this.name,
      this.slug,
      this.permalink,
      this.type,
      this.status,
      this.description,
      this.shortDescription,
      this.price,
      this.regularPrice,
      this.salePrice,
      this.stockQuantity,
      this.stockStatus,
      this.categories,
      this.images,
      this.vendor,
      this.storeName,
      this.store,
      this.sellerId,
      this.sellerName,
      this.sellerAvatar,
      this.colors,
      this.sizes,
      this.likes,
      this.shippable,
      this.localPickup,
      this.bestOffer,
      this.comments,
      this.liked,
      this.lLinks});

  ShopListData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    permalink = json['permalink'];
    type = json['type'];
    status = json['status'];
    description = json['description'];
    shortDescription = json['short_description'];
    price = json['price'];
    regularPrice = json['regular_price'];
    salePrice = json['sale_price'];
    stockQuantity = json['stock_quantity'];
    stockStatus = json['stock_status'];
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
    if (json['images'] != null) {
      images = new List<Images>();
      json['images'].forEach((v) {
        images.add(new Images.fromJson(v));
      });
    }
    vendor = json['vendor'];
    storeName = json['store_name'];
    store = json['store'] != null ? new Store.fromJson(json['store']) : null;
    sellerId = json['seller_id'];
    sellerName = json['seller_name'];
    sellerAvatar = json['seller_avatar'];
    if (json['colors'] != null) {
      colors = json['colors'].cast<String>();
    }
    sizes = json['sizes'].cast<String>();
    likes = json['likes'];
    shippable = json['shippable'];
    localPickup = json['local_pickup'];
    bestOffer = json['best_offer'];
    comments = json['comments'];
    liked = json['liked'];
    lLinks = json['_links'] != null ? new Links.fromJson(json['_links']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['permalink'] = this.permalink;
    data['type'] = this.type;
    data['status'] = this.status;
    data['description'] = this.description;
    data['short_description'] = this.shortDescription;
    data['price'] = this.price;
    data['regular_price'] = this.regularPrice;
    data['sale_price'] = this.salePrice;
    data['stock_quantity'] = this.stockQuantity;
    data['stock_status'] = this.stockStatus;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    if (this.images != null) {
      data['images'] = this.images.map((v) => v.toJson()).toList();
    }
    data['vendor'] = this.vendor;
    data['store_name'] = this.storeName;
    if (this.store != null) {
      data['store'] = this.store.toJson();
    }
    data['seller_id'] = this.sellerId;
    data['seller_name'] = this.sellerName;
    data['seller_avatar'] = this.sellerAvatar;
    if (this.colors != null) {
      data['colors'] = this.sizes;
    }
    data['sizes'] = this.sizes;
    data['likes'] = this.likes;
    data['shippable'] = this.shippable;
    data['local_pickup'] = this.localPickup;
    data['best_offer'] = this.bestOffer;
    data['comments'] = this.comments;
    data['liked'] = this.liked;
    if (this.lLinks != null) {
      data['_links'] = this.lLinks.toJson();
    }
    return data;
  }
}

class Categories {
  int id;
  String name;
  String slug;

  Categories({this.id, this.name, this.slug});

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    return data;
  }
}

class Images {
  int id;
  String dateCreated;
  String dateCreatedGmt;
  String dateModified;
  String dateModifiedGmt;
  String src;
  String name;
  String alt;

  Images(
      {this.id,
      this.dateCreated,
      this.dateCreatedGmt,
      this.dateModified,
      this.dateModifiedGmt,
      this.src,
      this.name,
      this.alt});

  Images.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    dateCreated = json['date_created'];
    dateCreatedGmt = json['date_created_gmt'];
    dateModified = json['date_modified'];
    dateModifiedGmt = json['date_modified_gmt'];
    src = json['src'];
    name = json['name'];
    alt = json['alt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['date_created'] = this.dateCreated;
    data['date_created_gmt'] = this.dateCreatedGmt;
    data['date_modified'] = this.dateModified;
    data['date_modified_gmt'] = this.dateModifiedGmt;
    data['src'] = this.src;
    data['name'] = this.name;
    data['alt'] = this.alt;
    return data;
  }
}

class Store {
  int id;
  String name;
  String shopName;
  String url;
  Address address;

  Store({this.id, this.name, this.shopName, this.url, this.address});

  Store.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    shopName = json['shop_name'];
    url = json['url'];
    address =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['shop_name'] = this.shopName;
    data['url'] = this.url;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    return data;
  }
}

class Address {
  String street1;
  String street2;
  String city;
  String zip;
  String country;
  String state;

  Address(
      {this.street1,
      this.street2,
      this.city,
      this.zip,
      this.country,
      this.state});

  Address.fromJson(Map<String, dynamic> json) {
    street1 = json['street_1'];
    street2 = json['street_2'];
    city = json['city'];
    zip = json['zip'];
    country = json['country'];
    state = json['state'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['street_1'] = this.street1;
    data['street_2'] = this.street2;
    data['city'] = this.city;
    data['zip'] = this.zip;
    data['country'] = this.country;
    data['state'] = this.state;
    return data;
  }
}

class Links {
  List<Self> self;
  List<CollectionData> collection;

  Links({this.self, this.collection});

  Links.fromJson(Map<String, dynamic> json) {
    if (json['self'] != null) {
      self = new List<Self>();
      json['self'].forEach((v) {
        self.add(new Self.fromJson(v));
      });
    }
    if (json['collection'] != null) {
      collection = new List<CollectionData>();
      json['collection'].forEach((v) {
        collection.add(new CollectionData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.self != null) {
      data['self'] = this.self.map((v) => v.toJson()).toList();
    }
    if (this.collection != null) {
      data['collection'] = this.collection.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Self {
  String href;

  Self({this.href});

  Self.fromJson(Map<String, dynamic> json) {
    href = json['href'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['href'] = this.href;
    return data;
  }
}

class CollectionData {
  String href;

  CollectionData({this.href});

  CollectionData.fromJson(Map<String, dynamic> json) {
    href = json['href'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['href'] = this.href;
    return data;
  }
}
