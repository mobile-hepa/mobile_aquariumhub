import 'package:aquariumhub/model/beans/ShopList.dart';

class ProductDetailsArgument {
  ShopListData shopListData;

  ProductDetailsArgument(this.shopListData);
}
