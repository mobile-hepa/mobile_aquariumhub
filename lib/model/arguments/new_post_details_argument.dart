import 'dart:io';

class NewPostDetailsArgument {
  File image;

  NewPostDetailsArgument(this.image);
}
