import 'package:aquariumhub/model/remote/requests/StripeTokenRequest.dart';
import 'package:aquariumhub/model/remote/response/StripeTokenResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/calls/StripeTokenDataCall.dart';
import 'package:rxdart/rxdart.dart';

class StripeTokenDataRepository {
  executeStripeTokenData(StripeTokenRequest stripeTokenRequest,
      BehaviorSubject<ApiResponse<StripeTokenResponse>> responseSubject) {
    new StripeTokenDataCall(stripeTokenRequest, responseSubject).execute("");
  }
}
