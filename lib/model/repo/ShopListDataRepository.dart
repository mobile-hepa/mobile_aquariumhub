import 'package:aquariumhub/model/remote/requests/ShopListRequest.dart';
import 'package:aquariumhub/model/remote/response/ShopListDataResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/calls/ShopListDataCall.dart';
import 'package:rxdart/rxdart.dart';

class ShopListDataRepository {
  executeShopListDataRepository(ShopListRequest ShopListRequest,
      BehaviorSubject<ApiResponse<ShopListDataResponse>> responseSubject) {
    new ShopListDataCall(ShopListRequest, responseSubject).execute();
  }
}
