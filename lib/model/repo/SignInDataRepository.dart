import 'package:aquariumhub/model/remote/requests/SignInRequest.dart';
import 'package:aquariumhub/model/remote/response/SignInDataResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/calls/SignInDataCall.dart';
import 'package:rxdart/rxdart.dart';

class SignInDataRepository {
  executeSignInData(SignInRequest SignInRequest,
      BehaviorSubject<ApiResponse<SignInDataResponse>> responseSubject) {
    new SignInDataCall(SignInRequest, responseSubject).execute("login");
  }
}
