import 'package:aquariumhub/model/remote/response/SignOutResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/calls/SignOutCall.dart';
import 'package:rxdart/rxdart.dart';

class SignOutRepository {
  executeSignOut(
      BehaviorSubject<ApiResponse<SignOutResponse>> responseSubject) {
    new SignOutCall(responseSubject).execute();
  }
}
