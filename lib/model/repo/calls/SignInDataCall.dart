import 'package:aquariumhub/model/remote/requests/SignInRequest.dart';
import 'package:aquariumhub/model/remote/response/SignInDataResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiService.dart';
import 'package:aquariumhub/model/remote/util/DataFetchCall.dart';
import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';

class SignInDataCall extends DataFetchCall<SignInDataResponse> {
  SignInRequest _request;

  SignInDataCall(SignInRequest request,
      BehaviorSubject<ApiResponse<SignInDataResponse>> responseSubject)
      : super(responseSubject) {
    this._request = request;
  }

  /// if return false then createApiAsyc is called
  /// if return true then loadFromDB Function  is called
  @override
  bool shouldFetchFromDB() {
    return false;
  }

  /// called when shouldFetchfromDB() is returning true
  @override
  void loadFromDB() {
    ///  get data from DB todo post/sinc on behaviourSubject after
  }

  /// called when shouldFetchfromDB() is returning false

  @override
  Future<Response> createApiAsync() {
    /// need to return APIService async task for API request
    return apiServiceInstance.SignIn(_request);
  }

  /// called when API Response is Success
  @override
  void onSuccess(SignInDataResponse response) {}

  /// called when API Response is success and need to parse JsonData to Model
  @override
  SignInDataResponse parseJson(Response response) {
    return SignInDataResponse.fromJson(response.data);
  }
}
