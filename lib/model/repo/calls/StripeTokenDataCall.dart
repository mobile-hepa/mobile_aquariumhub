import 'package:aquariumhub/model/remote/requests/StripeTokenRequest.dart';
import 'package:aquariumhub/model/remote/response/StripeTokenResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiService.dart';
import 'package:aquariumhub/model/remote/util/DataFetchCall.dart';
import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';

class StripeTokenDataCall extends DataFetchCall<StripeTokenResponse> {
  StripeTokenRequest _request;

  StripeTokenDataCall(StripeTokenRequest request,
      BehaviorSubject<ApiResponse<StripeTokenResponse>> responseSubject)
      : super(responseSubject) {
    this._request = request;
  }

  /// if return false then createApiAsyc is called
  /// if return true then loadFromDB Function  is called
  @override
  bool shouldFetchFromDB() {
    return false;
  }

  /// called when shouldFetchfromDB() is returning true
  @override
  void loadFromDB() {
    ///  get data from DB todo post/sinc on behaviourSubject after
  }

  /// called when shouldFetchfromDB() is returning false

  @override
  Future<Response> createApiAsync() {
    /// need to return APIService async task for API request
    return apiServiceInstance.StripeToken(_request);
  }

  /// called when API Response is Success
  @override
  void onSuccess(StripeTokenResponse response) {}

  /// called when API Response is success and need to parse JsonData to Model
  @override
  StripeTokenResponse parseJson(Response response) {
    return StripeTokenResponse.fromJson(response.data);
  }
}
