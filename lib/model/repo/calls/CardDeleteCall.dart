import 'package:aquariumhub/model/remote/requests/CardDeleteRequest.dart';
import 'package:aquariumhub/model/remote/response/CardDeleteResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiService.dart';
import 'package:aquariumhub/model/remote/util/DataFetchCall.dart';
import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';

class CardDeleteCall extends DataFetchCall<CardDeleteResponse> {
  CardDeleteRequest _request;

  CardDeleteCall(CardDeleteRequest request,
      BehaviorSubject<ApiResponse<CardDeleteResponse>> responseSubject)
      : super(responseSubject) {
    this._request = request;
  }

  /// if return false then createApiAsyc is called
  /// if return true then loadFromDB Function  is called
  @override
  bool shouldFetchFromDB() {
    return false;
  }

  /// called when shouldFetchfromDB() is returning true
  @override
  void loadFromDB() {
    ///  get data from DB todo post/sinc on behaviourSubject after
  }

  /// called when shouldFetchfromDB() is returning false

  @override
  Future<Response> createApiAsync() {
    /// need to return APIService async task for API request
    return apiServiceInstance.CardDelete(_request);
  }

  /// called when API Response is Success
  @override
  void onSuccess(CardDeleteResponse response) {}

  /// called when API Response is success and need to parse JsonData to Model
  @override
  CardDeleteResponse parseJson(Response response) {
    return CardDeleteResponse.fromJson(response.data);
  }
}
