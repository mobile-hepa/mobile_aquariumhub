import 'package:aquariumhub/model/remote/response/GetMyProfileResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiService.dart';
import 'package:aquariumhub/model/remote/util/DataFetchCall.dart';
import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';

class GetMyProfileCall extends DataFetchCall<GetMyProfileResponse> {
  GetMyProfileCall(
      BehaviorSubject<ApiResponse<GetMyProfileResponse>> responseSubject)
      : super(responseSubject) {}

  /// if return false then createApiAsyc is called
  /// if return true then loadFromDB Function  is called
  @override
  bool shouldFetchFromDB() {
    return false;
  }

  /// called when shouldFetchfromDB() is returning true
  @override
  void loadFromDB() {
    ///  get data from DB todo post/sinc on behaviourSubject after
  }

  /// called when shouldFetchfromDB() is returning false

  @override
  Future<Response> createApiAsync() {
    /// need to return APIService async task for API request
    return apiServiceInstance.GetMyProfile();
  }

  /// called when API Response is Success
  @override
  void onSuccess(GetMyProfileResponse response) {}

  /// called when API Response is success and need to parse JsonData to Model
  @override
  GetMyProfileResponse parseJson(Response response) {
    return GetMyProfileResponse.fromJson(response.data);
  }
}
