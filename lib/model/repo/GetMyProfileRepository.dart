import 'package:aquariumhub/model/remote/response/GetMyProfileResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/calls/GetMyProfileCall.dart';
import 'package:rxdart/rxdart.dart';

class GetMyProfileRepository {
  executeGetMyProfile(
      BehaviorSubject<ApiResponse<GetMyProfileResponse>> responseSubject) {
    new GetMyProfileCall(responseSubject).execute();
  }
}
