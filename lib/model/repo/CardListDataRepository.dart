import 'package:aquariumhub/model/remote/requests/CardListRequest.dart';
import 'package:aquariumhub/model/remote/response/CardListResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/calls/CardListDataCall.dart';
import 'package:rxdart/rxdart.dart';

class CardListDataRepository {
  executeCardListData(CardListRequest cardListRequest,
      BehaviorSubject<ApiResponse<CardListResponse>> responseSubject) {
    new CardListDataCall(cardListRequest, responseSubject).execute("");
  }
}
