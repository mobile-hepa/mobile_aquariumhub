import 'package:aquariumhub/model/remote/requests/StripeConnectRequest.dart';
import 'package:aquariumhub/model/remote/response/StripeConnectResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/calls/StripeConnectDataCall.dart';
import 'package:rxdart/rxdart.dart';

class StripeConnectDataRepository {
  executeStripeConnectData(StripeConnectRequest stripeConnectRequest,
      BehaviorSubject<ApiResponse<StripeConnectResponse>> responseSubject) {
    new StripeConnectDataCall(stripeConnectRequest, responseSubject)
        .execute("");
  }
}
