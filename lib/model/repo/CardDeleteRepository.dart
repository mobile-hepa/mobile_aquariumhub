import 'package:aquariumhub/model/remote/requests/CardDeleteRequest.dart';
import 'package:aquariumhub/model/remote/response/CardDeleteResponse.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/model/repo/calls/CardDeleteCall.dart';
import 'package:rxdart/rxdart.dart';

class CardDeleteRepository {
  executeCardDelete(CardDeleteRequest cardListRequest,
      BehaviorSubject<ApiResponse<CardDeleteResponse>> responseSubject) {
    new CardDeleteCall(cardListRequest, responseSubject).execute("");
  }
}
