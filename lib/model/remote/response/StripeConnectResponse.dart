import 'package:aquariumhub/model/beans/CardDetailsData.dart';
import 'package:aquariumhub/model/remote/response/GeneralResponse.dart';

class StripeConnectResponse extends GeneralResponse {
  CardDetailsData Result;

  StripeConnectResponse.fromJson(Map<String, dynamic> json)
      : Result = CardDetailsData.fromJson(json),
        super.fromJson(json);
}
