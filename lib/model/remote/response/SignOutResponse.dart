import 'package:aquariumhub/model/beans/CommonResponse.dart';
import 'package:aquariumhub/model/remote/response/GeneralResponse.dart';

class SignOutResponse extends GeneralResponse {
  CommonResponse Result;

  SignOutResponse.fromJson(Map<String, dynamic> json)
      : Result = CommonResponse.fromJson(json),
        super.fromJson(json);
}
