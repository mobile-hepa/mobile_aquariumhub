import 'package:aquariumhub/model/beans/SignInData.dart';

import 'GeneralResponse.dart';

class SignInDataResponse extends GeneralResponse {
  SignInData Result;

  SignInDataResponse.fromJson(Map<String, dynamic> json)
      : Result = SignInData.fromJson(json),
        super.fromJson(json);
}
