import 'package:aquariumhub/model/beans/CardDelete.dart';
import 'package:aquariumhub/model/remote/response/GeneralResponse.dart';

class CardDeleteResponse extends GeneralResponse {
  CardDelete Result;

  CardDeleteResponse.fromJson(Map<String, dynamic> json)
      : Result = CardDelete.fromJson(json),
        super.fromJson(json);
}
