import 'package:aquariumhub/model/beans/StripeData.dart';
import 'package:aquariumhub/model/remote/response/GeneralResponse.dart';

class StripeTokenResponse extends GeneralResponse {
  StripeData Result;

  StripeTokenResponse.fromJson(Map<String, dynamic> json)
      : Result = StripeData.fromJson(json),
        super.fromJson(json);
}
