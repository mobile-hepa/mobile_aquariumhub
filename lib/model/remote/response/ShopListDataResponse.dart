import 'package:aquariumhub/model/beans/ShopList.dart';

class ShopListDataResponse {
  ShopData Result;

  ShopListDataResponse.fromJson(List<dynamic> json)
      : Result = ShopData.fromJson(json);
}
