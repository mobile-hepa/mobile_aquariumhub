import 'package:aquariumhub/model/beans/UserProfile.dart';
import 'package:aquariumhub/model/remote/response/GeneralResponse.dart';

class GetMyProfileResponse extends GeneralResponse {
  UserProfile Result;

  GetMyProfileResponse.fromJson(Map<String, dynamic> json)
      : Result = UserProfile.fromJson(json),
        super.fromJson(json);
}
