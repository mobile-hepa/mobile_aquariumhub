import 'package:aquariumhub/model/beans/CardList.dart';
import 'package:aquariumhub/model/remote/response/GeneralResponse.dart';

class CardListResponse extends GeneralResponse {
  CardList Result;

  CardListResponse.fromJson(Map<String, dynamic> json)
      : Result = CardList.fromJson(json),
        super.fromJson(json);
}
