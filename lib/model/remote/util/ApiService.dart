import 'package:aquariumhub/model/remote/requests/CardDeleteRequest.dart';
import 'package:aquariumhub/model/remote/requests/CardListRequest.dart';
import 'package:aquariumhub/model/remote/requests/ShopListRequest.dart';
import 'package:aquariumhub/model/remote/requests/SignInRequest.dart';
import 'package:aquariumhub/model/remote/requests/StripeConnectRequest.dart';
import 'package:aquariumhub/model/remote/requests/StripeTokenRequest.dart';
import 'package:dio/dio.dart';

import '../ApiConstants.dart';
import 'NetworkUtil.dart';

/// write your all API Async requests here
class ApiService {
  NetworkUtil networkUtil = NetworkUtil();

  // SignIn
  Future<Response> SignIn(SignInRequest signInRequest) {
    return networkUtil.post(
        ApiConstants.ADMIN + ApiConstants.SIGN_IN, signInRequest.toMap());
  }

  // GetShopData
  Future<Response> Products(ShopListRequest shopListRequest) {
    return networkUtil.get(ApiConstants.JSON, shopListRequest.toMap());
  }

  // GetMyProfile
  Future<Response> GetMyProfile() {
    return networkUtil.get(ApiConstants.ADMIN + ApiConstants.GET_PROFILE);
  }

  // SignOut
  Future<Response> SignOut() {
    return networkUtil.post(ApiConstants.ADMIN + ApiConstants.SIGN_OUT);
  }

  // StripeToken
  Future<Response> StripeToken(StripeTokenRequest stripeTokenRequest) {
    return networkUtil.post(ApiConstants.StripeAdmin +
        stripeTokenRequest
            .toMap()
            .toString()
            .replaceAll("{", "")
            .replaceAll("}", "")
            .replaceAll(":", "=")
            .replaceAll(",", "&")
            .replaceAll(" ", ""));
  }

  //StripeConnect
  Future<Response> StripeConnect(StripeConnectRequest stripeConnectRequest) {
    return networkUtil.post(ApiConstants.ADMIN, stripeConnectRequest.toMap());
  }

  //CardData
  Future<Response> CardData(CardListRequest cardListRequest) {
    return networkUtil.post(ApiConstants.ADMIN, cardListRequest.toMap());
  }

  //CardDelete
  Future<Response> CardDelete(CardDeleteRequest cardDeleteRequest) {
    return networkUtil.post(ApiConstants.ADMIN, cardDeleteRequest.toMap());
  }
}

///Single final Object of API Service
final apiServiceInstance = ApiService();
