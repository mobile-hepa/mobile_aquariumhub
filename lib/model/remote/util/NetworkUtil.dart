import 'package:aquariumhub/main.dart';
import 'package:dio/dio.dart';

import '../ApiConstants.dart';

class NetworkUtil {
  Dio _dio;

  NetworkUtil() {
    ///Create Dio Object using baseOptions set receiveTimeout,connectTimeout
    BaseOptions options = BaseOptions();
    options.baseUrl = ApiConstants.BASE_URL;
    options.contentType = "application/x-www-form-urlencoded";
    options.headers["Authorization"] = ApiConstants.Authorization;
    if (getAuthHeaders.length != 0) {
      options.headers["Cookie"] = "{Cookie: " + getAuthHeaders + "}";
    }
    _dio = Dio(options);
    _dio.interceptors.add(LogInterceptor());
  }

  ///used for calling Get Request
  Future<Response> get(String url, [Map<String, dynamic> params]) async {
    BaseOptions options = BaseOptions();
    options.baseUrl = ApiConstants.BASE_URL;
    options.contentType = "application/x-www-form-urlencoded";
    options.headers["Authorization"] = ApiConstants.Authorization;
    if (getAuthHeaders.length != 0) {
      options.headers["Cookie"] = "{Cookie: " + getAuthHeaders + "}";
    }
    _dio = Dio(options);

    Response response = await _dio.get(url,
        queryParameters: params,
        options: Options(responseType: ResponseType.json));
    return response;
  }

  ///used for calling post Request
  Future<Response> post(String url, [Map<String, dynamic> params]) async {
    BaseOptions options = BaseOptions();
    options.baseUrl = ApiConstants.BASE_URL;
    options.contentType = "application/x-www-form-urlencoded";
    options.headers["Authorization"] = ApiConstants.Authorization;
    if (getAuthHeaders.length != 0) {
      options.headers["Cookie"] = "{Cookie: " + getAuthHeaders + "}";
    }
    _dio = Dio(options);
    Response response = await _dio.post(url,
        data: params, options: Options(responseType: ResponseType.json));
    return response;
  }

  Future<Response> postImage(String url, FormData params) async {
    print(url);
    Response response = await _dio.post(url,
        data: params, options: Options(responseType: ResponseType.json));
    return response;
  }

}
