class ApiResponse<ResultType> {
  Status status;
  ResultType data;
  Error error;
  dynamic cookieData;

  ApiResponse(Status status, ResultType data, Error error, dynamic cookieData) {
    this.status = status;
    this.data = data;
    this.error = error;
    this.cookieData = cookieData;
  }

  static ApiResponse loading<ResultType>() {
    return new ApiResponse<ResultType>(Status.LOADING, null, null, null);
  }

  static ApiResponse success<ResultType>(ResultType data,
      [dynamic cookieData]) {
    return new ApiResponse<ResultType>(Status.SUCCESS, data, null, cookieData);
  }

  static ApiResponse failed<ResultType>(Error error) {
    return new ApiResponse<ResultType>(Status.ERROR, null, error, null);
  }
}

class Error {
  int statusCode;
  String errorMessage;
  String errorBody;

  Error(this.statusCode, this.errorMessage, this.errorBody);
}

enum Status { LOADING, SUCCESS, ERROR }
