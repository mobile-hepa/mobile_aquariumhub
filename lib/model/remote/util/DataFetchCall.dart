import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';

import 'ApiResponse.dart';

abstract class DataFetchCall<ResultType> {
  bool shouldFetchFromDB();

  void loadFromDB();

  Future<Response> createApiAsync();

  void onSuccess(ResultType response);

  ResultType parseJson(Response response);

  BehaviorSubject<ApiResponse<ResultType>> observable;

  DataFetchCall(BehaviorSubject<ApiResponse<ResultType>> observable) {
    this.observable = observable;
  }

  void execute([String isPass]) {
    if (shouldFetchFromDB()) {
      loadFromDB();
    } else {
      var key;
      if (isPass != null) {
        key = isPass;
      } else {
        key = "";
      }
      observable.sink.add(ApiResponse.loading<ResultType>());
      _getResponse(key).then((onValue) {
        observable.sink.add(onValue);
      });
    }
  }

  Future<ApiResponse<ResultType>> _getResponse(String keyPass) async {
    try {
      var headerdata = "";
      Response response = await createApiAsync();
      print("Response : ${response.data}");

      if (response.statusCode >= 200 && response.statusCode <= 299) {
        if (keyPass == "login") {
          String rawCookie = response.headers["set-cookie"].toString();
          // NOTE: Split it in key=value pairs
          headerdata = rawCookie;
        }

        ResultType responseModel = parseJson(response);
        onSuccess(responseModel);

        if (keyPass == "login") {
          return ApiResponse.success<ResultType>(responseModel, headerdata);
        } else {
          return ApiResponse.success<ResultType>(responseModel);
        }
      } else {
        return ApiResponse.failed<ResultType>(Error(response.statusCode,
            response.data.toString(), response.data.toString()));
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return ApiResponse.failed<ResultType>(
          Error(500, error.toString(), stacktrace.toString()));
    }
  }

}
