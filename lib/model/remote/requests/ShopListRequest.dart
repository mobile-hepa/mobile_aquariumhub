import 'dart:core';

import '../ApiConstants.dart';

class ShopListRequest {
  int page;

  ShopListRequest(this.page);

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map();
    map[ApiConstants.ConsumerKey] = ApiConstants.ConsumerKeyValue;
    map[ApiConstants.ConsumerSecret] = ApiConstants.ConsumerSecretValue;
    map[ApiConstants.Lang] = "en";
    map[ApiConstants.page] = page;
    map[ApiConstants.status] = ApiConstants.publish;
    map[ApiConstants.StockStatus] = ApiConstants.InStock;
    print("ShopListRequest: " + map.toString());
    return map;
  }
}
