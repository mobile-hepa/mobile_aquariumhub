import 'dart:core';

import '../ApiConstants.dart';

class SignInRequest {
  String UserName;
  String Password;
  String Lang;

  SignInRequest(this.UserName, this.Password, this.Lang);

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map();
    map[ApiConstants.UserName] = UserName;
    map[ApiConstants.Password] = Password;
    map[ApiConstants.Lang] = Lang;
    print("SignInRequest: " + map.toString());
    return map;
  }
}
