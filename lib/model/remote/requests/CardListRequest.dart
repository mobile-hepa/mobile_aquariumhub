import 'dart:core';

import '../ApiConstants.dart';

class CardListRequest {
  String action;
  String subaction;

  CardListRequest(this.action, this.subaction);

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map();
    map[ApiConstants.action] = action;
    map[ApiConstants.subAction] = subaction;
    print("CardListRequest: " + map.toString());
    return map;
  }
}
