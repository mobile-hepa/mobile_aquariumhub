import 'dart:core';

import '../ApiConstants.dart';

class StripeTokenRequest {
  String key;
  String cardNumber;
  int cardCvc;
  int cardExpMonth;
  int cardExpYear;
  String cardCurrency;

  StripeTokenRequest(this.key, this.cardNumber, this.cardCvc, this.cardExpMonth,
      this.cardExpYear, this.cardCurrency);

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map();
    map[ApiConstants.key] = key;
    map[ApiConstants.cardNumber] = cardNumber;
    map[ApiConstants.cardCvc] = cardCvc;
    map[ApiConstants.cardExpMonth] = cardExpMonth;
    map[ApiConstants.cardExpYear] = cardExpYear;
    map[ApiConstants.cardCurrency] = cardCurrency;
    print("StripeTokenRequest: " + map.toString());
    return map;
  }
}
