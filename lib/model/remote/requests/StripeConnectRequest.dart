import 'dart:core';

import '../ApiConstants.dart';

class StripeConnectRequest {
  String token_alt;
  String token;
  String action;
  String subaction;

  StripeConnectRequest(this.token_alt, this.token, this.action, this.subaction);

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map();
    map[ApiConstants.sritpeTokenAlt] = token_alt;
    map[ApiConstants.sritpeToken] = token;
    map[ApiConstants.action] = action;
    map[ApiConstants.subAction] = subaction;
    print("StripeConnectRequest: " + map.toString());
    return map;
  }
}
