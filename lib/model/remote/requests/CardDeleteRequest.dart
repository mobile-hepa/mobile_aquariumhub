import 'dart:core';

import '../ApiConstants.dart';

class CardDeleteRequest {
  String token_id;

  CardDeleteRequest(this.token_id);

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map();
    map[ApiConstants.action] = ApiConstants.STRIPE_CONNECT;
    map[ApiConstants.subAction] = ApiConstants.removeRawCard;
    map[ApiConstants.tokenId] = token_id;
    print("CardDeleteRequest: " + map.toString());
    return map;
  }
}
