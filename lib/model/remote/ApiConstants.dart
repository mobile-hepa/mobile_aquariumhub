class ApiConstants {
  static final String BASE_URL = "https://aquariumhub.com/";

  //admin
  static final String ADMIN = "wp-admin/admin-ajax.php?action=";

  //Stripe

  static final String StripeAdmin = "https://api.stripe.com/v1/tokens?";

  //json
  static final String JSON = "wp-json/wc/v3/products";

  //Authorization
  static final String Authorization = "Basic MWEzdXBlck1AbjoxYTN1cGVyTUBu";

  //key
  static final String ConsumerKeyValue =
      'ck_018980c57d5ba2f28b41588b1c7f7680490f9fc6';
  static final String ConsumerSecretValue =
      'cs_0d93a80f795ebbd7c7d91b4a7d4b5a268352d57f';

  //stripeKey
  static final String Token_altValue =
      "pk_test_gzaFMVw67weNxOe4fbJhQXrt00qIKiVc4B";
  static final String Token_Value =
      "pk_test_ZREk9T4axmP0S4R1rUsvcI5C00graI4oN3";

  //api name
  static final String SIGN_IN = "mstoreapp-login";
  static final String PRODUCTS = "products";
  static final String SIGN_OUT = "mstoreapp-logout";
  static final String GET_PROFILE = "dokan_load_my_store";
  static final String STRIPE_CONNECT = "stripe_connect";

  // parameter
  static final String UserName = 'username';
  static final String Password = 'password';
  static final String Lang = 'lang';
  static final String page = 'page';
  static final String status = 'status';
  static final String publish = 'publish';
  static final String ConsumerKey = 'consumer_key';
  static final String ConsumerSecret = 'consumer_secret';
  static final String StockStatus = 'stock_status';
  static final String InStock = 'instock';
  static final String TimeOnPage = 'time_on_page';
  static final String key = 'key';
  static final String cardNumber = 'card[number]';
  static final String cardCvc = 'card[cvc]';
  static final String cardExpMonth = 'card[exp_month]';
  static final String cardExpYear = 'card[exp_year]';
  static final String cardCurrency = 'card[currency]';
  static final String sritpeTokenAlt = 'token_alt';
  static final String sritpeToken = 'token';
  static final String action = 'action';
  static final String subAction = 'subaction';
  static final String saveTokens = 'save_tokens';
  static final String getCards = 'get_cards';
  static final String tokenId = 'token_id';
  static final String removeRawCard = 'remove_raw_card';
}
