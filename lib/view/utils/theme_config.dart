import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:flutter/material.dart';

ThemeData themeData() {
  return ThemeData(primaryColor: primaryColor, primarySwatch: secondaryColor);
}
