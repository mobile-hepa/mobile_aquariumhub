import 'package:aquariumhub/view/ui/card/cardList.dart';
import 'package:aquariumhub/view/ui/dashboard/dashboard_screen.dart';
import 'package:aquariumhub/view/ui/forgot_password/forgotpassword.dart';
import 'package:aquariumhub/view/ui/otp/otp_screen.dart';
import 'package:aquariumhub/view/ui/payment_method/add_card.dart';
import 'package:aquariumhub/view/ui/product_details/product_details_screen.dart';
import 'package:aquariumhub/view/ui/search/search_screen.dart';
import 'package:aquariumhub/view/ui/shopping_cart/shoppingcart.dart';
import 'package:aquariumhub/view/ui/sign_in/signin.dart';
import 'package:aquariumhub/view/ui/sign_up/signup.dart';
import 'package:aquariumhub/view/ui/splash/splash.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../ui/product_details/product_details_screen.dart';

var routes = <String, WidgetBuilder>{
  // initial route
  DashboardTag: (BuildContext context) => DashboardScreen(),
  SplashScreenPageTag: (BuildContext context) => SplashScreen(),
  SignInScreenPageTag: (BuildContext context) => SignInScreen(),
  SignUpScreenTag: (BuildContext context) => SignUpScreen(),
  ProductDetailsScreenTag: (BuildContext context) => ProductDetailPage(),
  ForgotPasswordscreenTag: (BuildContext context) => ForgotPasswordscreen(),
  ShoppingCartScreenTag: (BuildContext context) => ShoppingCartScreen(),
  OtpPageTag: (BuildContext context) => OtpScreen(),
  SearchTag: (BuildContext context) => SearchPage(),
  AddCardPageTag: (BuildContext context) => AddCardPage(),
  CardDetailsTag: (BuildContext context) => CardDetails(),
};

