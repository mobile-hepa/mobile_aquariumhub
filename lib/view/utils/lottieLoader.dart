import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

lottieEmptyWidget() {
  return Container(child: Lottie.asset('assets/json/aquarium.json'));
}

lottieLoaderWidget() {
  return Container(
      child: Lottie.asset('assets/json/fishloader.json', height: 100));
}
