import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedData {
  // SESSION DATA
  static isUserLoggedIn(value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool(isUserLoginKey, value);
  }

  static readUserLoggedIn() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getBool(isUserLoginKey) != null) {
      return prefs.getBool(isUserLoginKey);
    } else {
      return false;
    }
  }

  // SAVE COOKIE
  static saveCookieferences(value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(cookieKey, value);
  }

  // READ COOKIE
  static readCookiePreferences() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getString(cookieKey) != null) {
      return prefs.getString(cookieKey);
    } else {
      return "";
    }
  }

  // SAVE USER DATA
  static saveUserPreferences(value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
//    preferences.setString(userKey, json.encode(value));
  }

  // READ USER DATA
  static readUserPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    //   return json.decode(prefs.getString(userKey));
  }
}
