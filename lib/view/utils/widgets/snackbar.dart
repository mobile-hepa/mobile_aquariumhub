
import 'package:flutter/material.dart';

void showSnackBar(BuildContext context, String textMessage) {
  final scaffold = Scaffold.of(context);
  scaffold.showSnackBar(
    SnackBar(
      content: Text(textMessage),
      action: SnackBarAction(
          label: 'CANCEL', onPressed: scaffold.hideCurrentSnackBar),
    ),
  );
}