import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/text_view.dart';
import 'package:flutter/material.dart';

class SimpleRaisedButton extends StatelessWidget {
  String text;
  VoidCallback onTap;

  SimpleRaisedButton({this.text, this.onTap});

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      height: 40,
      minWidth: double.infinity,
      child: RaisedButton(
        onPressed: onTap,
        child: TextView(
          text,
          textColor: backgroundColor,
        ),
      ),
    );
  }
}
