import 'package:aquariumhub/view/utils/widgets/responsive_ui.dart';
import 'package:flutter/material.dart';

import '../Constants.dart';

class CustomTextField extends StatefulWidget {
  final String hint;
  final TextEditingController textEditingController;
  final TextInputType keyboardType;
  final bool obscureText;
  final IconData icon;
  final double borderRadius;
  final bool isPassword;
  final TextInputAction action;

  CustomTextField(
      {this.hint,
      this.textEditingController,
      this.keyboardType,
      this.icon,
      this.obscureText = false,
      this.borderRadius,
        this.action = TextInputAction.next,
      this.isPassword = false});

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  double _width;
  double _pixelRatio;
  bool large;
  bool medium;
  bool isPassToogle = false;

  @override
  Widget build(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);

    return Material(
      borderRadius: BorderRadius.circular(widget.borderRadius ?? 30),
      elevation: large ? 8 : (medium ? 10 : 8),
      child: TextFormField(
        obscureText: widget.isPassword ? !isPassToogle : false,
        controller: widget.textEditingController,
        keyboardType: widget.keyboardType,
        cursorColor: primaryColor,
        textInputAction: widget.action,
        decoration: InputDecoration(
          prefixIcon: Icon(widget.icon, color: primaryColor, size: 20),
          hintText: widget.hint,
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(widget.borderRadius ?? 30),
              borderSide: BorderSide.none),
          suffixIcon: widget.isPassword
              ? IconButton(
                  icon: Icon(
                    isPassToogle ? Icons.visibility_off : Icons.visibility,
                    color: primaryColor,
                  ),
                  onPressed: () {
                    setState(() {
                      isPassToogle = !isPassToogle;
                    });
                  },
                )
              : null,
        ),
      ),
    );
  }
}
