import 'package:flutter/material.dart';

imageWidget({String image, double height, double width, bool loading}) {
  Image _image = new Image.network(
    image,
  );

  return _image.image.resolve(ImageConfiguration()).addListener(
    ImageStreamListener(
      (info, call) {
        loading = false;
        // do something
      },
    ),
  );
}
