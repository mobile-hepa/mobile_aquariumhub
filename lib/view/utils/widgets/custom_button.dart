import 'package:aquariumhub/view/utils/SizeConfig.dart';
import 'package:flutter/material.dart';

import '../Constants.dart';

class CustomRaisedButton extends StatelessWidget {
  VoidCallback onTap;
  String title;
  double width;
  double height;
  double fontSize;
  double _pixelRatio;
  bool large;
  bool medium;
  Color bgColor = whiteColor;
  Color textColor = blackColor;

  CustomRaisedButton({
    @required this.title,
    this.width,
    this.height,
    @required this.fontSize,
    @required this.onTap,
    this.bgColor,
    this.textColor
  });

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      onPressed: onTap,
      textColor: Colors.white,
      padding: EdgeInsets.all(0.0),
      child: Container(
        alignment: Alignment.center,
        width: width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          gradient: LinearGradient(
            colors: <Color>[primaryColor, secondaryColor],
          ),
        ),
        padding: const EdgeInsets.all(12.0),
        child: Text(title, style: TextStyle(fontSize: fontSize)),
      ),
    );
      /*RaisedButton(
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(getProportionateScreenHeight(30))),
        onPressed: onTap,
        textColor: Colors.white,
        padding: EdgeInsets.all(0.0),
        child: Container(
          height: (height== null) ? getProportionateScreenHeight(60) : height,
          alignment: Alignment.center,
          width: width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(getProportionateScreenHeight(30))),
            color: bgColor,
          ),
          child: Text(title, style: TextStyle(fontSize: fontSize, color: textColor, fontFamily: 'Inter-Regular')),
        ),
      );*/
  }
}
