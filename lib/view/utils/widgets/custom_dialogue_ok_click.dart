import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:flutter/material.dart';

class CustomDialogueOneBtn extends StatelessWidget {
  String title;
  String message;
  String btnName;
  VoidCallback ontap;

  CustomDialogueOneBtn(
      {@required this.title,
      @required this.message,
      @required this.btnName,
      @required this.ontap});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      elevation: 0,
      backgroundColor: backgroundColor,
      child: contentBox(context),
    );
  }

  contentBox(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                  width: double.infinity,
                  height: 40,
                  decoration: BoxDecoration(
                      color: primaryColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(16),
                        topRight: Radius.circular(16),
                      )),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      title,
                      style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: backgroundColor),
                    ),
                  )),
              SizedBox(
                height: 35,
              ),
              Text(
                message,
                style: TextStyle(fontSize: 14),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 22,
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                    onPressed: ontap,
                    child: Text(
                      btnName,
                      style: TextStyle(fontSize: 18),
                    )),
              ),
            ],
          ),
        )
      ],
    );
  }
}
