import 'package:aquariumhub/view/utils/widgets/responsive_ui.dart';
import 'package:flutter/material.dart';

import '../Constants.dart';

class SquareTextField extends StatefulWidget {
  final String hint;
  final TextEditingController textEditingController;
  final TextInputType keyboardType;
  final bool obscureText;
  final IconData icon;
  final double borderRadius;
  final bool isPassword;
  final TextInputAction action;
  final bool isDropDown;

  SquareTextField({
    this.hint,
    this.textEditingController,
    this.keyboardType,
    this.icon,
    this.obscureText = false,
    this.borderRadius,
    this.action = TextInputAction.next,
    this.isPassword = false,
    this.isDropDown = false,
  });

  @override
  _SquareTextFieldState createState() => _SquareTextFieldState();
}

class _SquareTextFieldState extends State<SquareTextField> {
  double _width;
  double _pixelRatio;
  bool large;
  bool medium;
  bool isPassToogle = false;

  @override
  Widget build(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);

    return Material(
      child: TextFormField(
        obscureText: widget.isPassword ? !isPassToogle : false,
        controller: widget.textEditingController,
        keyboardType: widget.keyboardType,
        cursorColor: primaryColor,
        textInputAction: widget.action,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          hintText: widget.hint,
          hintStyle: TextStyle(color: gray600Color),
          border: OutlineInputBorder(),
            suffixIcon: widget.isDropDown ? Icon(Icons.arrow_drop_down) : null),
      ),
    );
  }
}
