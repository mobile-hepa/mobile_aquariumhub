import 'package:flutter/material.dart';

import '../text_view.dart';

class CustomDropdown extends StatefulWidget {
  String hintText;
  List<String> dataList;
  String selectedValue;

  CustomDropdown({this.hintText, this.dataList, this.selectedValue});

  @override
  _CustomDropdownState createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  String selectedValues;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: DropdownButton(
        value: selectedValues,
        hint: TextView(
          widget.hintText,
          fontSize: 16,
        ),
        items: widget.dataList.map((categoryVal) {
          return DropdownMenuItem(
            child: Container(
                width: MediaQuery.of(context).size.width - 68,
                child: TextView(categoryVal)),
            value: categoryVal,
          );
        }).toList(),
        onChanged: (value) {
          setState(() {
            selectedValues = value;
          });
        },
      ),
    );
  }
}
