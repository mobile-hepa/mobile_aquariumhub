import 'package:aquariumhub/bloc/ShopListDataBloc.dart';
import 'package:aquariumhub/model/arguments/product_details_argument.dart';
import 'package:aquariumhub/model/beans/ShopList.dart';
import 'package:aquariumhub/model/remote/requests/ShopListRequest.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/lottieLoader.dart';
import 'package:aquariumhub/view/utils/text_view.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share/share.dart';

class ShopList extends StatefulWidget {
  @override
  _ShopListState createState() => _ShopListState();
}

class _ShopListState extends State<ShopList> {
  double _height;
  double _width;
  TextEditingController searchControlller = TextEditingController();
  ShopListDataBloc _shopListDataBloc = ShopListDataBloc();
  List<ShopListData> shopListData = [];
  ScrollController controller;
  int _offset = 1;
  bool _isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    controller = new ScrollController(
      initialScrollOffset: 0.0,
      keepScrollOffset: true,
    )..addListener(_scrollListener);

    if (_offset == 1) {
      _getData(offset: _offset, from: "initstate");
    } else {
      print("No call");
    }

    super.initState();
  }

  @override
  void dispose() {
    controller.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {
    if (controller.offset >= controller.position.maxScrollExtent &&
        !controller.position.outOfRange) {
      if (!_isLoading) {
        _offset = _offset + 1;
        _getData(offset: _offset, from: "scroll");
      }
    }
    if (controller.offset <= controller.position.minScrollExtent &&
        !controller.position.outOfRange) {
      print("reach the top");
    }
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Container(
      color: backgroundColor,
      width: _width,
      height: _height,
      padding: EdgeInsets.all(16),
      child: Builder(
        builder: (context) => Column(
          children: [
            gridViewPosts(),
          ],
        ),
      ),
    );
  }

  Widget gridViewPosts() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: GridView.builder(
          controller: controller,
          itemCount: shopListData.length,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          physics: AlwaysScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, childAspectRatio: 0.55),
          itemBuilder: (context, index) {
            //return cardPost(index);
            if (shopListData.length - 1 == index && _isLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return cardPost(index);
            }
          },
        ),
      ),
    );
  }

  Widget cardPost(int index) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, ProductDetailsScreenTag,
            arguments: ProductDetailsArgument(shopListData[index]));
      },
      child: Container(
        width: _width / 2,
        //  height: 300,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
        child: Card(
          color: whiteColor,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: SizedBox(
                  height: _width / 2.5,
                  width: _width,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8)),
                    child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl: shopListData[index].images[0].src,
                        placeholder: (context, url) => lottieLoaderWidget()),
                  ),
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: TextView(
                  shopListData[index].name,
                  fontSize: 15,
                  fontFamily: 'SemiBold',
                  textColor: grayDarkColor,
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: TextView(
                  shopListData[index].shortDescription,
                  textColor: grayColor,
                  fontFamily: 'Regular',
                  fontSize: 14,
                  maxLines: 1,
                  textOverflow: TextOverflow.ellipsis,
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      'assets/image/profile.png',
                      width: 30,
                      height: 30,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    TextView(
                      shopListData[index].storeName,
                      fontFamily: 'SemiBold',
                      textColor: blueColor,
                      fontSize: 12,
                    )
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    TextView(
                      "\$" + shopListData[index].price,
                      fontFamily: 'SemiBold',
                      fontSize: 14,
                      textColor: blackColor,
                    ),
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      child: SizedBox(),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Icon(
                        FontAwesomeIcons.heart,
                        color: redColor,
                        size: 18,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Icon(
                        FontAwesomeIcons.commentDots,
                        color: grayLightColor,
                        size: 20,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Share.share(
                          shopListData[index].permalink,
                          subject: appTitle,
                        );
                      },
                      child: Icon(
                        FontAwesomeIcons.shareSquare,
                        color: greyLightColor,
                        size: 18,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Share.share(
                          shopListData[index].permalink,
                          subject: appTitle,
                        );
                      },
                      child: Icon(
                        FontAwesomeIcons.shoppingCart,
                        color: greyLightColor,
                        size: 18,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  children: [
                    TextView(
                      shopListData[index].sizes.length != 0
                          ? sizeColonText + shopListData[index].sizes[0]
                          : sizeColonText + '-',
                      fontSize: 14,
                      fontFamily: 'Light',
                      textColor: grayColor,
                    ),
                  ],
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }

  void _getData({@required int offset, @required from}) async {
    setState(() {
      _isLoading = true;
    });

    _shopListDataBloc.executeShopListDataRepository(ShopListRequest(offset));

    _shopListDataBloc.subject.listen((value) {
      if (value.status == Status.LOADING) {
        EasyLoading.show(status: 'loading...');
      } else if (value.status == Status.SUCCESS) {
        if (_isLoading) {
          EasyLoading.dismiss();
          shopListData.addAll(value.data.Result.data);
        }
        setState(() {
          _isLoading = false;
        });
      } else if (value.status == Status.ERROR) {
        EasyLoading.dismiss();
      }
    });
  }
}
