import 'dart:math';

import 'package:aquariumhub/main.dart';
import 'package:aquariumhub/view/ui/account/account_screen.dart';
import 'package:aquariumhub/view/ui/dashboard/signin_ask_screen.dart';
import 'package:aquariumhub/view/ui/feed/feed_screen.dart';
import 'package:aquariumhub/view/ui/sell/sell_page.dart';
import 'package:aquariumhub/view/ui/shop_list/shop_list.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:provider/provider.dart';

import 'menu_page.dart';

class DashboardScreen extends StatefulWidget {
  static List<MenuItem> mainMenu = [
    MenuItem(feedText, Icons.supervisor_account_outlined, 0),
    MenuItem(shopText, Icons.segment, 1),
    MenuItem(sellText, Icons.camera_alt, 2),
    MenuItem(newsText, Icons.notifications, 3),
    MenuItem(accountText, Icons.person, 4),
  ];

  @override
  _DashboardScreenState createState() => new _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final _drawerController = ZoomDrawerController();
  int _currentPage = 0;

  NavigateShoppingCartScreen() {
    Navigator.pushNamed(context, ShoppingCartScreenTag);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ZoomDrawer(
      controller: _drawerController,
      menuScreen: MenuScreen(
        DashboardScreen.mainMenu,
        callback: _updatePage,
        close: _drawerClose,
        current: _currentPage,
      ),
      mainScreen: MainScreen(),
      borderRadius: 24.0,
//      showShadow: true,
      angle: 0.0,
      slideWidth:
          MediaQuery.of(context).size.width * (ZoomDrawer.isRTL() ? .45 : 0.65),
      // openCurve: Curves.fastOutSlowIn,
      // closeCurve: Curves.bounceIn,
    );
  }

  void _drawerClose() {
    _drawerController.toggle();
  }

  void _updatePage(index) {
    Provider.of<MenuProvider>(context, listen: false).updateCurrentPage(index);
    _drawerController.toggle();
  }
}

class MainScreen extends StatefulWidget {

  MainScreen();

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    final rtl = ZoomDrawer.isRTL();
    return ValueListenableBuilder<DrawerState>(
      valueListenable: ZoomDrawer.of(context).stateNotifier,
      builder: (context, state, child) {
        return child;
      },
      // AbsorbPointer(
      //   absorbing: state != DrawerState.closed,
      //   child: child,
      // );
      child: GestureDetector(
        child: PageStructure(),
        onPanUpdate: (details) {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
          if (details.delta.dx < 6 && !rtl || details.delta.dx < -6 && rtl) {
            ZoomDrawer.of(context).toggle();
          }
        },
      ),
    );
  }
}

class MenuProvider extends ChangeNotifier {
  int _currentPage = 0;

  int get currentPage => _currentPage;

  void updateCurrentPage(int index) {
    if (index != currentPage) {
      _currentPage = index;
      notifyListeners();
    }
  }
}

class PageStructure extends StatelessWidget {
  final String title;
  final Widget child;
  final List<Widget> actions;
  final Color backgroundColor;
  final double elevation;

  const PageStructure({
    Key key,
    this.title,
    this.child,
    this.actions,
    this.backgroundColor,
    this.elevation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> _widgetOptions = <Widget>[
      ScrollFeed(),
      // HomeScreen(),
      //TestLazyLoading(),
      ShopList(),
      SellPage(),
      Text('News In Progress', style: TextStyle(color: Colors.pink)),
      isLogin ? AccountPage() : SignInAskScreen(),
    ];
    final angle = ZoomDrawer.isRTL() ? 180 * pi / 180 : 0.0;
    final _currentPage =
        context.select<MenuProvider, int>((provider) => provider.currentPage);
    final container = Container(
      color: Colors.grey[300],
      child: Center(
        child: Text(
            "Current Page : ${DashboardScreen.mainMenu[_currentPage].title}"),
      ),
    );
    final color = primaryColor;
    final style = TextStyle(color: color);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: primaryColor,
          automaticallyImplyLeading: false,
          title: Text(
            DashboardScreen.mainMenu[_currentPage].title,
          ),
          leading: Transform.rotate(
            angle: angle,
            child: PlatformIconButton(
              icon: Icon(
                Icons.menu,
                color: Colors.white,
              ),
              onPressed: () {
                FocusScopeNode currentFocus = FocusScope.of(context);
                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
                ZoomDrawer.of(context).toggle();
              },
            ),
          ),
          actions: [
            _currentPage == 0
                ? IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () {
                  Navigator.pushNamed(context, ShoppingCartScreenTag);
                })
                : SizedBox()
          ],
          //     trailingActions: actions,
        ),
        bottomNavigationBar: PlatformNavBar(
          currentIndex: _currentPage,
          itemChanged: (index) =>
              Provider.of<MenuProvider>(context, listen: false)
                  .updateCurrentPage(index),
          items: DashboardScreen.mainMenu
              .map(
                (item) => BottomNavigationBarItem(
              title: Text(
                item.title,
                style: style,
              ),
              icon: Icon(
                item.icon,
                color: color,
              ),
            ),
          )
              .toList(),
        ),
        body: _widgetOptions.elementAt(_currentPage));
  }
}
