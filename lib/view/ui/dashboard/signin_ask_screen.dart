import 'package:aquariumhub/view/ui/dashboard/dashboard_screen.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/text_view.dart';
import 'package:aquariumhub/view/utils/widgets/custom_button.dart';
import 'package:aquariumhub/view/utils/widgets/responsive_ui.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SignInAskScreen extends StatefulWidget {
  @override
  _SignInAskScreenState createState() => _SignInAskScreenState();
}

class _SignInAskScreenState extends State<SignInAskScreen> {
  bool _large;
  bool _medium;
  double _height;
  double _width;
  double _pixelRatio;

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    _large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    _medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);
    return Scaffold(
      body: Container(
        height: _height,
        width: _width,
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextView(needAccountText,
                fontSize: 14, textAlign: TextAlign.center),
            SizedBox(height: 14),
            ChangeNotifierProvider(
                create: (_) => MenuProvider(),
                builder: (context, child) => CustomRaisedButton(
                      title: signInBtn,
                      width: 200,
                      fontSize: _large ? 14 : (_medium ? 12 : 10),
                      onTap: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            SignInScreenPageTag,
                            (Route<dynamic> route) => false);
                      },
                    )),
            ChangeNotifierProvider(
                create: (_) => MenuProvider(),
                builder: (context, child) => CustomRaisedButton(
                      title: signUpBTN,
                      width: 200,
                      fontSize: _large ? 14 : (_medium ? 12 : 10),
                      onTap: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            SignUpScreenTag, (Route<dynamic> route) => false);
                      },
                    )),
          ],
        )),
      ),
    );
  }
}
