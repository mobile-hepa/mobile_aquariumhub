import 'dart:io';

import 'package:aquariumhub/bloc/SignOutBloc.dart';
import 'package:aquariumhub/main.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/shared_data.dart';
import 'package:aquariumhub/view/utils/widgets/custom_dialogue_ok_click.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class MenuScreen extends StatefulWidget {
  final List<MenuItem> mainMenu;
  final Function(int) callback;
  final Function close;
  final int current;

  MenuScreen(this.mainMenu, {Key key, this.callback, this.close, this.current});

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  SignOutBloc _signOutBloc = SignOutBloc();

  final widthBox = SizedBox(
    width: 16.0,
  );

  @override
  void initState() {
    // TODO: implement initState
    _signOutBloc.subject.listen((value) {
      if (value.status == Status.LOADING) {
        EasyLoading.show(status: 'loading...');
      } else if (value.status == Status.SUCCESS) {
        EasyLoading.dismiss();
        signOutCall(context);
      } else if (value.status == Status.ERROR) {}
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle androidStyle = const TextStyle(
        fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white);
    final TextStyle iosStyle = const TextStyle(color: Colors.white);
    final style = kIsWeb
        ? androidStyle
        : Platform.isAndroid
            ? androidStyle
            : iosStyle;

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(begin: Alignment.topLeft, colors: [
              primaryColor,
              secondaryColor,
            ])),
        child: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 24.0, left: 24.0, right: 24.0),
                child: Container(
                  width: 80,
                  height: 80,
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 36.0, left: 24.0, right: 24.0),
                child: Text(
                  appTitle,
                  style: TextStyle(
                    fontSize: 22,
                    color: Colors.white,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ListTile(
                    title: Text(
                      messageText,
                      style: TextStyle(color: backgroundColor, fontSize: 16),
                    ),
                    onTap: () {
                      widget.close();
                    },
                  ),
                  ListTile(
                    title: Text(
                      ordersText,
                      style: TextStyle(color: backgroundColor, fontSize: 16),
                    ),
                    onTap: () {
                      widget.close();
                    },
                  ),
                  ListTile(
                    title: Text(
                      accountSettingText,
                      style: TextStyle(color: backgroundColor, fontSize: 16),
                    ),
                    onTap: () {
                      widget.close();
                      if (isLogin) {
                        Navigator.pushNamed(context, CardDetailsTag);
                      } else {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return CustomDialogueOneBtn(
                                title: appTitle,
                                message: needAccountText,
                                btnName: okBTN,
                                ontap: () {
                                  Navigator.pop(context);
                                },
                              );
                            });
                      }
                    },
                  ),
                  ListTile(
                    title: Text(
                      shippingAddressText,
                      style: TextStyle(color: backgroundColor, fontSize: 16),
                    ),
                    onTap: () {
                      widget.close();
                    },
                  ),
                ],
              ),
              Spacer(),
              isLogin
                  ? Padding(
                padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                child: OutlineButton(
                  child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            signOutText,
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                        borderSide: BorderSide(color: Colors.white, width: 2.0),
                        onPressed: () {
                          _signOutBloc.executeSignOutData();
                        },
                        textColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16.0)),
                      ),
              )
                  : Padding(
                padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                child: OutlineButton(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      signInText,
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                  onPressed: () {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        SignInScreenPageTag,
                            (Route<dynamic> route) => false);
                  },
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16.0)),
                ),
                    ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}

signOutCall(BuildContext context) async {
  getAuthHeaders = null;
  SharedData.isUserLoggedIn(false);
  SharedData.saveCookieferences(null);
  isLogin = false;
  Navigator.of(context).pushNamedAndRemoveUntil(
      SignInScreenPageTag, (Route<dynamic> route) => false);
}

class MenuItemWidget extends StatelessWidget {
  final MenuItem item;
  final Widget widthBox;
  final TextStyle style;
  final Function callback;
  final Function close;
  final bool selected;

  final white = Colors.white;

  const MenuItemWidget(
      {Key key,
      this.item,
      this.widthBox,
      this.style,
      this.callback,
      this.close,
      this.selected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () {
        // close();
      },
      // onPressed: () => callback(item.index),
      color: selected ? Color(0x44000000) : null,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            item.icon,
            color: white,
            size: 24,
          ),
          widthBox,
          Expanded(
            child: Text(
              item.title,
              style: style,
            ),
          )
        ],
      ),
    );
  }
}

class MenuItem {
  final String title;
  final IconData icon;
  final int index;

  const MenuItem(this.title, this.icon, this.index);
}
