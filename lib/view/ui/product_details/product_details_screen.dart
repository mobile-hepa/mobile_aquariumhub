import 'package:aquariumhub/model/arguments/product_details_argument.dart';
import 'package:aquariumhub/model/beans/ShopList.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/SizeConfig.dart';
import 'package:aquariumhub/view/utils/lottieLoader.dart';
import 'package:aquariumhub/view/utils/text_view.dart';
import 'package:aquariumhub/view/utils/widgets/post_button.dart';
import 'package:aquariumhub/view/utils/widgets/responsive_ui.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:share/share.dart';

class ProductDetailPage extends StatefulWidget {
  ProductDetailPage({Key key}) : super(key: key);
  final int count = 0;

  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage>
    with TickerProviderStateMixin {
  double _height;
  double _width;
  double _pixelRatio;
  bool _large;
  bool _medium;

  bool isLiked = true;
  List<RadioModel> sampleData = new List<RadioModel>();
  List<String> shippingData = new List<String>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sampleData.add(new RadioModel(false, 'A'));
    sampleData.add(new RadioModel(false, 'B'));
    sampleData.add(new RadioModel(false, 'C'));
    sampleData.add(new RadioModel(false, 'D'));

    shippingData.add("\$7,11 Expedited (1-3 day) Shipping in all orders");
    shippingData.add("HUB protect available");
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    _large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    _medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);

    final ProductDetailsArgument productDetailsArgument =
        ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: whiteColor,
      appBar: _appBar(),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  _itemImage(productDetailsArgument.shopListData.images[0].src),
                  _detailWidget(productDetailsArgument.shopListData),
                ],
              ),
            ),
          ),
          _bottomView(productDetailsArgument.shopListData)
        ],
      ),
    );
  }

  Widget _appBar() {
    return AppBar(
      iconTheme: IconThemeData(
        color: gray600Color, //change your color here
      ),
      title: TextView(
        'Listing Details',
        textColor: blackColor,
        fontFamily: 'Regular',
        fontSize: 16,
        maxLines: 1,
        textOverflow: TextOverflow.ellipsis,
      ),
      backgroundColor: whiteColor,
      actions: <Widget>[
        IconButton(
            icon: Icon(Icons.share),
            color: gray600Color,
            onPressed: () {
              //Navigator.pushNamed(context, ShoppingCartScreenTag);
            }),
        PopupMenuButton<String>(
          // onSelected: handleClick,
          icon: Icon(
            Icons.more_vert,
            color: gray600Color,
          ),
          itemBuilder: (BuildContext context) {
            return {'Logout', 'Settings'}.map((String choice) {
              return PopupMenuItem<String>(
                value: choice,
                child: Text(choice),
              );
            }).toList();
          },
        ),
      ],
    );
  }

  Widget _itemImage(String src) {
    return CachedNetworkImage(
      imageUrl: src,
      placeholder: (context, url) => (Center(
        child: lottieLoaderWidget(),
      )),
      fit: BoxFit.cover,
      height: getProportionateScreenHeight(250),
      width: double.infinity,
    );
  }

  Widget _detailWidget(ShopListData shopListData) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10)
          .copyWith(bottom: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          SizedBox(height: 10),
          TextView(shopListData.name, fontSize: 22),
          SizedBox(
            height: 15,
          ),
          _availableSize(shopListData),
          SizedBox(
            height: 12,
          ),
          _detailList(shopListData),
          //categoryList()
          _shippingDetail(),
          _likeCommentShare(),
          _horizontalDivider(),
          _userLikes(),
          _findSimilar(),
        ],
      ),
    );
  }

  Widget _bottomView(ShopListData shopListData) {
    return Container(
      color: blackColor.withOpacity(0.85),
      child: Row(
        children: [
          SizedBox(
            width: 20,
          ),
          TextView(
            "\$" + shopListData.price,
            textColor: whiteColor,
            fontSize: 20,
          ),
          SizedBox(
            width: 10,
          ),
          TextView("\$" + shopListData.price,
              textColor: gray600Color,
              fontSize: 16,
              textDecoration: TextDecoration.lineThrough),
          Spacer(),
          TextView(offerText,
              textColor: orangeColor,
              fontSize: 18,
              textDecoration: TextDecoration.underline),
          SizedBox(
            width: 20,
          ),
          FlatButton(
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
            height: getProportionateScreenHeight(60),
            padding: EdgeInsets.symmetric(horizontal: 35),
            color: primaryColor,
            textColor: whiteColor,
            child: TextView(buyNowTextBTN.toUpperCase()),
            onPressed: () {},
          )
        ],
      ),
    );
  }

  Widget _availableSize(ShopListData shopListData) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              TextView(
                dollar + shopListData.salePrice,
                textColor: blackColor,
                fontSize: 15,
                fontWeight: FontWeight.w500,
              ),
              SizedBox(
                width: 5,
              ),
              TextView("\$" + shopListData.regularPrice,
                  textColor: gray600Color,
                  fontSize: 13,
                  textDecoration: TextDecoration.lineThrough),
              _verticalDivider(),
              TextView(
                  shopListData.sizes.length != 0
                      ? sizeColonText + shopListData.sizes[0]
                      : sizeColonText + '-',
                  textColor: primaryColor,
                  fontSize: 15),
              _verticalDivider(),
              TextView(
                  shopListData.categories.length != 0
                      ? shopListData.categories[0].name
                      : '',
                  textColor: primaryColor,
                  fontSize: 15),
            ],
          ),
        )
      ],
    );
  }

  Widget _verticalDivider({Color color = orangeColor, double horizontalPadding = 20.0}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
      child: Container(
        width: 2,
        height: 12,
        color: color,
      ),
    );
  }

  Widget _detailList(ShopListData shopListData) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 3),
          child: Row(
            children: [
              Image.asset(
                "assets/image/fish.png",
                color: primaryColor.withOpacity(0.3),
                height: 15,
              ),
              SizedBox(
                width: 10,
              ),
              TextView(shopListData.name)
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 3),
          child: Row(
            children: [
              Image.asset(
                "assets/image/fish.png",
                color: primaryColor.withOpacity(0.3),
                height: 15,
              ),
              SizedBox(
                width: 10,
              ),
              TextView(
                shopListData.sizes.length != 0
                    ? sizeColonText + shopListData.sizes[0]
                    : sizeColonText + '-',
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 3),
          child: Row(
            children: [
              Image.asset(
                "assets/image/fish.png",
                color: primaryColor.withOpacity(0.3),
                height: 15,
              ),
              SizedBox(
                width: 10,
              ),
              TextView(shopListData.colors.length != 0
                  ? colorColonText + shopListData.colors[0]
                  : colorColonText + '-')
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 3),
          child: Row(
            children: [
              Image.asset(
                "assets/image/fish.png",
                color: primaryColor.withOpacity(0.3),
                height: 15,
              ),
              SizedBox(
                width: 10,
              ),
              TextView("Feeds on Shriimp")
            ],
          ),
        )
      ],
    );
  }

  Widget categoryList() {
    return ListView.builder(
      itemCount: sampleData.length,
      //scrollDirection:Axis.horizontal ,
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 50,
          child: new InkWell(
            //highlightColor: Colors.red,
            splashColor: Colors.blueAccent,
            onTap: () {
              setState(() {
                sampleData.forEach((element) => element.isSelected = false);
                sampleData[index].isSelected = true;
              });
            },
            child: new RadioItem(sampleData[index]),
          ),
        );
      },
    );
  }

  Widget _shippingDetail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 20,
        ),
        TextView(
          offerTexProtectionText,
          textColor: primaryColor,
          fontSize: 18,
          fontWeight: FontWeight.w500,
        ),
        _horizontalDivider(),
        _shippingList()
      ],
    );
  }

  Widget _horizontalDivider({Color color = gray600Color, double padding = 10}) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: padding),
      child: Container(
        width: double.infinity,
        height: 0.3,
        color: color,
      ),
    );
  }

  Widget _shippingList() {
    return ListView.builder(
      itemCount: shippingData.length,
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return _shippingListItem(index);
      },
    );
  }

  Widget _shippingListItem([int index]) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: Column(
        children: [
          Row(
            children: [
              Icon(
                Icons.lock,
                color: gray600Color.withOpacity(0.5),
                size: 15,
              ),
              SizedBox(
                width: 10,
              ),
              TextView(
                shippingData[index],
                textColor: gray600Color,
              )
            ],
          ),
          _horizontalDivider(padding: 10)
        ],
      ),
    );
  }

  Widget _likeCommentShare() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        PostButton(
          icon: Icon(
            MdiIcons.heartOutline,
            color: gray600Color,
            size: 20.0,
          ),
          label: likeText,
          onTap: () => print('Like'),
        ),
        PostButton(
          icon: Icon(
            MdiIcons.commentOutline,
            color: gray600Color,
            size: 20.0,
          ),
          label: commentText,
          onTap: () => print('Comment'),
        ),
        PostButton(
            icon: Icon(
              MdiIcons.shareOutline,
              color: gray600Color,
              size: 20.0,
            ),
            label: shareText,
            onTap: () {
              Share.share(
                "https://suryadevsingh24032000.medium.com/",
                subject: "Follow Me",
              );
            })
      ],
    );
  }

  Widget _userLikes() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          CachedNetworkImage(
              imageUrl: "https://randomuser.me/api/portraits/women/49.jpg",
              imageBuilder: (context, imageProvider) => Container(
                height: getProportionateScreenHeight(30),
                width: getProportionateScreenWidth(30),
                decoration: BoxDecoration(
                  borderRadius: new BorderRadius.all(
                      Radius.circular(getProportionateScreenHeight(15))),
                  image: DecorationImage(
                      image: imageProvider, fit: BoxFit.cover),
                ),
              )),
          SizedBox(
            width: 10,
          ),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              children: [
                TextSpan(
                  text: "Mark Hemlin ",
                  style: TextStyle(color: primaryColor, fontSize: 15),
                ),
                TextSpan(
                  text: "and ",
                  style: TextStyle(color: blackColor, fontSize: 15),
                ),
                TextSpan(
                  text: "4 others ",
                  style: TextStyle(color: primaryColor, fontSize: 15),
                ),
                TextSpan(
                  text: "like this",
                  style: TextStyle(color: blackColor, fontSize: 15),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _findSimilar() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 30,
        ),
        TextView(
          findSimilarText.toUpperCase(),
          textColor: gray600Color,
          fontSize: 16,
          fontWeight: FontWeight.w500,
        ),
        SizedBox(
          height: 30,
        ),
        _bySimilar("By Size"),
        _horizontalDivider(),
        _bySimilar("By Color"),
        _horizontalDivider(),
        _bySimilar("By Category"),
        SizedBox(
          height: 10,
        )
      ],
    );
  }

  Widget _bySimilar(String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        TextView(
          title,
          textColor: gray600Color,
          fontSize: 16,
          fontWeight: FontWeight.w500,
        ),
        Icon(
          Icons.chevron_right,
          color: gray600Color,
          size: 30,
        ),
      ],
    );
  }
}

class RadioModel {
  bool isSelected;
  final String text;

  RadioModel(this.isSelected, this.text);
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;

  RadioItem(this._item);

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.all(15.0),
      child: new Container(
        height: 50.0,
        width: 50.0,
        child: new Center(
          child: new Text(_item.text,
              style: new TextStyle(
                  color: _item.isSelected ? Colors.white : Colors.black,
                  //fontWeight: FontWeight.bold,
                  fontSize: 18.0)),
        ),
        decoration: new BoxDecoration(
          color: _item.isSelected ? Colors.blueAccent : Colors.transparent,
          border: new Border.all(
              width: 1.0,
              color: _item.isSelected ? Colors.blueAccent : Colors.grey),
          borderRadius: const BorderRadius.all(const Radius.circular(2.0)),
        ),
      ),
    );
  }
}
