import 'dart:io';

import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/text_view.dart';
import 'package:aquariumhub/view/utils/widgets/custom_dropdown.dart';
import 'package:aquariumhub/view/utils/widgets/responsive_ui.dart';
import 'package:aquariumhub/view/utils/widgets/simple_raised_button.dart';
import 'package:aquariumhub/view/utils/widgets/square_text_field.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class SellPage extends StatefulWidget {
  @override
  _SellPageState createState() => _SellPageState();
}

enum ShippingValues { shippble, local_pickup }

class _SellPageState extends State<SellPage> {
  double _height;
  double _width;
  double _pixelRatio;
  bool _large;
  bool _medium;
  File _image;
  final picker = ImagePicker();

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController categoryController = TextEditingController();
  TextEditingController subCategoryController = TextEditingController();
  TextEditingController quantityController = TextEditingController();
  TextEditingController sizeController = TextEditingController();
  TextEditingController colorController = TextEditingController();
  TextEditingController listingPriceController = TextEditingController();
  TextEditingController earningController = TextEditingController();
  ShippingValues type = ShippingValues.shippble;
  List<String> category = [
    'Freshwater Fish',
    'Saltwater Fish',
    'Coral',
    'Clean Up Crews',
    'Invertebrates',
    'Equipment',
    'Supplies'
  ];
  List<String> subCategory = [
    'Angelfish',
    'Betta Fish',
    'Bichir',
    'Bloodfin Tetra',
    'Cherry Barb',
    'Discus',
    'Gold Fish'
  ];
  List<String> quantity = ['1', '2', '3', '4', '5', '6'];
  List<String> size = ['N/A', 'S', 'M', 'L', 'XL', 'XXL'];
  List<String> color = [
    'N/A',
    'Black',
    'White',
    'Blue',
    'Grey',
    'Pink',
    'Green',
    'Purple',
    'Orange',
    'Yellow'
  ];

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    _large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    _medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 50,
            ),
            _itemForSellText(),
            SizedBox(
              height: 25,
            ),
            _descriptionText(),
            SizedBox(
              height: 25,
            ),
            _uploadPhoto(),
            _showImage(),
            _productVideo(),
            _titleData(),
            _descriptionData(),
            _categoryDetailsData(),
            _pricingDetails(),
            _shippingDetails(),
            _radioButtons(),
            _saveButton()
          ],
        ),
      ),
    );
  }

  Widget _uploadPhoto() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 25),
      child: _uploadImageContainer(() {
        libraryImage();
      },
          Icon(
            Icons.camera_alt,
            size: 50,
            color: black26,
          ),
          uploadPhotosText),
    );
  }

  Widget imageWidget() {
    return Center(
      child: Container(
        height: _height / 1.8,
        width: _width - 40,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
          child: Image.asset('assets/image/upload.png'),
        ),
      ),
    );
  }

  Future videoPicker() async {
    final pickedFile = await picker.getVideo(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        // Navigator.pushNamed(context, DetailScreenTag,
        //     arguments: NewPostDetailsArgument(_image));
      } else {
        print(noImageText);
      }
    });
  }

  Future libraryImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        // Navigator.pushNamed(context, DetailScreenTag,
        //     arguments: NewPostDetailsArgument(_image));
      } else {
        print(noImageText);
      }
    });
  }

  Widget _itemForSellText() {
    return Center(
      child: TextView(
        itemForSellText,
        fontWeight: FontWeight.w500,
        fontSize: 24,
      ),
    );
  }

  Widget _descriptionText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Center(
          child: TextView(
        decriptionNewPostText,
        textAlign: TextAlign.center,
        fontSize: 16,
      )),
    );
  }

  Widget _uploadImageContainer(
      GestureTapCallback onTap, Icon icons, String uploadText) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: backgroundColor, //Color(0xfff3f3f3),
        height: 200,
        width: double.maxFinite,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              icons,
              TextView(
                uploadText,
                fontSize: 9,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _showImage() {
    return SizedBox(
      height: 110,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: 8,
        itemBuilder: (context, index) {
          return Container(
              margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              height: 100,
              child: Image.asset(
                'assets/image/aquarium.png',
              ));
        },
      ),
    );
  }

  Widget _productVideo() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: TextView(
              addProductVideoText,
              fontSize: 20,
            ),
          ),
          SizedBox(
            height: 16,
          ),
          _uploadImageContainer(() {
            videoPicker();
          },
              Icon(
                Icons.videocam,
                size: 50,
                color: black26,
              ),
              addVideosText)
        ],
      ),
    );
  }

  Widget _titleData() {
    return _commonTitleAndDescription(
      title: titleText,
      controller: titleController,
      hintText: addProdcutHintText,
      titleDescription: titleDescriptionText,
    );
  }

  Widget _descriptionData() {
    return _commonTitleAndDescription(
      title: descriptionText,
      titleDescription: descriptionLableText,
      controller: descriptionController,
      hintText: descriptionHintText,
    );
  }

  Widget _categoryDetailsData() {
    return CategoryDetailsWidget(
      title: categoryText,
      titleDescription: categoryDescriptionText,
      categoryController: categoryController,
      subCategoryController: subCategoryController,
      quantityController: quantityController,
      sizeController: sizeController,
      colorController: colorController,
      category: category,
      subCategory: subCategory,
      quantity: quantity,
      size: size,
      color: color,
    );
  }

  Widget _pricingDetails() {
    return ProductPriceWidget(
      title: productPriceText,
      listingPriceController: listingPriceController,
      earningController: earningController,
    );
  }

  Widget _shippingDetails() {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Align(
        alignment: Alignment.centerLeft,
        child: TextView(
          shippingText,
          fontSize: 20,
        ),
      ),
    );
  }

  Widget _radioButtons() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextView(
                shippableText,
                fontSize: 20,
              ),
              Radio(
                value: ShippingValues.shippble,
                groupValue: type,
                onChanged: (ShippingValues value) {
                  setState(() {
                    type = value;
                  });
                },
              ),
            ],
          ),
          Divider(
            height: 2,
            color: gray600Color,
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextView(
                localPickupText,
                fontSize: 20,
              ),
              Radio(
                value: ShippingValues.local_pickup,
                groupValue: type,
                onChanged: (ShippingValues value) {
                  setState(() {
                    type = value;
                  });
                },
              ),
            ],
          ),
          Divider(
            height: 2,
            color: gray600Color,
          ),
        ],
      ),
    );
  }

  Widget _saveButton() {
    return Padding(
      padding: const EdgeInsets.only(left: 16, top: 45, bottom: 16, right: 16),
      child: SimpleRaisedButton(
        text: saveBTN,
        onTap: () {},
      ),
    );
  }
}

/*

//Product Price class

*/

class ProductPriceWidget extends StatelessWidget {
  String title;
  TextEditingController listingPriceController;
  TextEditingController earningController;

  ProductPriceWidget(
      {this.title, this.listingPriceController, this.earningController});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          Align(
              alignment: Alignment.centerLeft,
              child: TextView(
                title,
                fontSize: 20,
              )),
          SizedBox(
            height: 5,
          ),
          SquareTextField(
            textEditingController: listingPriceController,
            hint: title,
          ),
          SizedBox(
            height: 10,
          ),
          SquareTextField(
            textEditingController: earningController,
            hint: title,
          ),
        ],
      ),
    );
  }
}

/*

// Category data class

*/

class CategoryDetailsWidget extends StatefulWidget {
  String title;
  String titleDescription;
  TextEditingController categoryController;
  TextEditingController subCategoryController;
  TextEditingController quantityController;
  TextEditingController sizeController;
  TextEditingController colorController;
  List<String> category;
  List<String> subCategory;
  List<String> quantity;
  List<String> size;
  List<String> color;

  CategoryDetailsWidget(
      {this.title,
      this.titleDescription,
      this.categoryController,
      this.subCategoryController,
      this.quantityController,
      this.sizeController,
      this.colorController,
      this.category,
      this.subCategory,
      this.quantity,
      this.size,
      this.color});

  @override
  _CategoryDetailsWidgetState createState() => _CategoryDetailsWidgetState();
}

class _CategoryDetailsWidgetState extends State<CategoryDetailsWidget> {
  String selectedCategory;
  String selectedSubcategory;
  String selectedQuantity;
  String selectedSize;
  String selectedColor;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Column(
        children: [
          Align(
              alignment: Alignment.centerLeft,
              child: TextView(
                widget.title,
                fontSize: 20,
              )),
          SizedBox(
            height: 5,
          ),
          Align(
              alignment: Alignment.centerLeft,
              child: TextView(
                widget.titleDescription,
                fontSize: 14,
                textColor: grayColor,
              )),
          SizedBox(
            height: 5,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: black26, width: 1)),
            child: CustomDropdown(
              hintText: categoryText,
              dataList: widget.category,
              selectedValue: selectedCategory,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: black26, width: 1)),
            child: CustomDropdown(
              hintText: subCategoryHintText,
              dataList: widget.subCategory,
              selectedValue: selectedSubcategory,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: black26, width: 1)),
            child: CustomDropdown(
              hintText: productQuantityHintText,
              dataList: widget.quantity,
              selectedValue: selectedQuantity,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: black26, width: 1)),
            child: CustomDropdown(
              hintText: productSizeHintText,
              dataList: widget.size,
              selectedValue: selectedSize,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: black26, width: 1)),
            child: CustomDropdown(
              hintText: productColorHintText,
              dataList: widget.color,
              selectedValue: selectedColor,
            ),
          ),
        ],
      ),
    );
  }
}

/*

// Common Styling class for title and description

*/

class _commonTitleAndDescription extends StatelessWidget {
  String title;
  String titleDescription;
  TextEditingController controller;
  String hintText;

  _commonTitleAndDescription(
      {this.title, this.titleDescription, this.controller, this.hintText});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Column(
        children: [
          Align(
              alignment: Alignment.centerLeft,
              child: TextView(
                title,
                fontSize: 20,
              )),
          SizedBox(
            height: 5,
          ),
          Align(
              alignment: Alignment.centerLeft,
              child: TextView(
                titleDescription,
                fontSize: 14,
                textColor: grayLightColor,
              )),
          SquareTextField(
            textEditingController: controller,
            hint: hintText,
          )
        ],
      ),
    );
  }
}
