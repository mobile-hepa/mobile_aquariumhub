/*
import 'package:aquariumhub/bloc/WorkerCategoryDataBloc.dart';
import 'package:aquariumhub/model/beans/WorkerCategoryData.dart';
import 'package:aquariumhub/model/remote/requests/WorkerCategoryRequest.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/view/utils/text_view.dart';
import 'package:flutter/material.dart';

class WorkerCategoriesPage extends StatefulWidget {
  WorkerCategoriesPage();

  @override
  _WorkerCategoriesPageState createState() => _WorkerCategoriesPageState();
}

class _WorkerCategoriesPageState extends State<WorkerCategoriesPage> {
  bool isloading = true;
  List<WorkerData> categoryList;
  WorkerCategoryDataBloc _workerCategoryDataBloc = WorkerCategoryDataBloc();

  @override
  void initState() {
    _workerCategoryDataBloc.executeWorkerCategoryData(WorkerCategoryRequest(1));

    _workerCategoryDataBloc.subject.listen((value) {
      if (value.status == Status.SUCCESS) {
        categoryList = value.data.Result.dataList;
        setState(() {
          isloading = false;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: isloading
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                padding: EdgeInsets.all(10),
                shrinkWrap: true,
                itemCount: categoryList.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                      child: Card(
                    elevation: 5,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        //height: 1500.0,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: [
                                      TextView(
                                          categoryList[index]
                                                  .workertypeId
                                                  .toString() +
                                              " : ",
                                          fontFamily: 'Poppins-SemiBold',
                                          fontSize: 12),
                                      TextView(categoryList[index].workerType,
                                          fontFamily: 'Poppins-Regular',
                                          fontSize: 12),
                                    ],
                                  )
                                ],
                              ),
                            ]),
                      ),
                    ),
                  ));
                },
              ));
  }
}
*/
