import 'dart:async';

import 'package:aquariumhub/view/ui/dashboard/dashboard_screen.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/widgets/responsive_ui.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  AnimationController _sliderAnimationController;
  Animation<double> _sliderAnimation;
  bool showMessage = false;
  double _height;
  double _width;
  double _pixelRatio;
  bool _large;
  bool _medium;

  @override
  void initState() {
    super.initState();
    startTime();
  }

  startTime() async {
    var _duration = new Duration(seconds: 2);
    Future.delayed(Duration(milliseconds: 800), () {
      setState(() {
        showMessage = true;
      });
    });

    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.of(context).pushReplacementNamed(DashboardTag);
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    _large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    _medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);
    return ChangeNotifierProvider(
      create: (_) => MenuProvider(),
      builder: (context, child) => Center(
          child: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/image/slide2.jpg'),
                fit: BoxFit.cover)),
        child: Stack(
          alignment: Alignment.center,
          children: [
            AnimatedPositioned(
              duration: Duration(milliseconds: 800),
              left: showMessage ? (_width / 8) : -(_width / 1.2),
              child: Image.asset(
                'assets/image/logo_light.png',
                width: (_width - 75),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
