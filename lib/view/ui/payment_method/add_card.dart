import 'package:aquariumhub/bloc/StripeConnectDataBloc.dart';
import 'package:aquariumhub/bloc/StripeTokenDataBloc.dart';
import 'package:aquariumhub/model/remote/ApiConstants.dart';
import 'package:aquariumhub/model/remote/requests/StripeConnectRequest.dart';
import 'package:aquariumhub/model/remote/requests/StripeTokenRequest.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/input_formatters.dart';
import 'package:aquariumhub/view/utils/payment_card.dart';
import 'package:aquariumhub/view/utils/widgets/custom_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class AddCardPage extends StatefulWidget {
  @override
  _AddCardPageState createState() => _AddCardPageState();
}

class _AddCardPageState extends State<AddCardPage> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  var _formKey = new GlobalKey<FormState>();
  var numberController = new TextEditingController();
  var _paymentCard = PaymentCard();
  var _autoValidate = false;
  var _card = new PaymentCard();
  var token_alt = "";
  var token = "";
  StripeTokenDataBloc _stripeTokenDataBloc = StripeTokenDataBloc();
  StripeConnectDataBloc _stripeConnectDataBloc = StripeConnectDataBloc();

  @override
  void initState() {
    super.initState();
    _paymentCard.type = CardType.Others;
    numberController.addListener(_getCardTypeFrmNumber);

    _stripeTokenDataBloc.subject.listen((value) {
      if (value.status == Status.LOADING) {
        EasyLoading.show(status: 'loading...');
      } else if (value.status == Status.SUCCESS) {
        EasyLoading.dismiss();
        setState(() {
          if (token_alt == "") {
            token_alt = value.data.Result.id;
            _stripeTokenDataBloc.executeStripeTokenData(StripeTokenRequest(
                ApiConstants.Token_Value,
                _paymentCard.number,
                _paymentCard.cvv,
                _paymentCard.month,
                _paymentCard.year,
                'usd'));
          } else {
            token = value.data.Result.id;
            _stripeConnectDataBloc.executeStripeTokenData(StripeConnectRequest(
                token_alt,
                token,
                ApiConstants.STRIPE_CONNECT,
                ApiConstants.saveTokens));
          }
        });
      } else if (value.status == Status.ERROR) {
        EasyLoading.dismiss();
      }
    });

    _stripeConnectDataBloc.subject.listen((value) {
      if (value.status == Status.LOADING) {
        EasyLoading.show(status: 'loading...');
      } else if (value.status == Status.SUCCESS) {
        EasyLoading.dismiss();
        if (value.data.Result.success == false) {
          EasyLoading.showError(value.data.Result.data.info);
          //showSnackBar(context, "Invalid credentials");
          Future.delayed(const Duration(milliseconds: 1000), () {
            EasyLoading.dismiss();
          });
        }
      } else if (value.status == Status.ERROR) {
        EasyLoading.showError(value.data.Result.data.info);
        //showSnackBar(context, "Invalid credentials");
        Future.delayed(const Duration(milliseconds: 1000), () {
          EasyLoading.dismiss();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          title: new Text(paymentMethodText),
        ),
        body: new Container(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: new Form(
              key: _formKey,
              autovalidate: _autoValidate,
              child: new ListView(
                children: <Widget>[
                  new SizedBox(
                    height: 20.0,
                  ),
                  new TextFormField(
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                      new LengthLimitingTextInputFormatter(19),
                      new CardNumberInputFormatter()
                    ],
                    controller: numberController,
                    decoration: new InputDecoration(
                      border: const UnderlineInputBorder(),
                      filled: true,
                      icon: CardUtils.getCardIcon(_paymentCard.type),
                      hintText: writtenCardText,
                      labelText: numberText,
                    ),
                    onSaved: (String value) {
                      print('onSaved = $value');
                      print('Num controller has = ${numberController.text}');
                      _paymentCard.number = CardUtils.getCleanedNumber(value);
                    },
                    validator: CardUtils.validateCardNum,
                  ),
                  new SizedBox(
                    height: 30.0,
                  ),
                  new TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                      new LengthLimitingTextInputFormatter(4),
                    ],
                    decoration: new InputDecoration(
                      border: const UnderlineInputBorder(),
                      filled: true,
                      icon: new Image.asset(
                        'assets/image/card_cvv.png',
                        width: 30.0,
                        color: Colors.grey[600],
                      ),
                      hintText: numberBehindText,
                      labelText: cvvText,
                    ),
                    validator: CardUtils.validateCVV,
                    keyboardType: TextInputType.number,
                    onSaved: (value) {
                      _paymentCard.cvv = int.parse(value);
                    },
                  ),
                  new SizedBox(
                    height: 30.0,
                  ),
                  new TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                      new LengthLimitingTextInputFormatter(4),
                      new CardMonthInputFormatter()
                    ],
                    decoration: new InputDecoration(
                      border: const UnderlineInputBorder(),
                      filled: true,
                      icon: new Image.asset(
                        'assets/image/calender.png',
                        width: 30.0,
                        color: Colors.grey[600],
                      ),
                      hintText: MMYYText,
                      labelText: expiryDateText,
                    ),
                    validator: CardUtils.validateDate,
                    keyboardType: TextInputType.number,
                    onSaved: (value) {
                      List<int> expiryDate = CardUtils.getExpiryDate(value);
                      _paymentCard.month = expiryDate[0];
                      _paymentCard.year = expiryDate[1];
                    },
                  ),
                  new SizedBox(
                    height: 50.0,
                  ),
                  new Container(
                    alignment: Alignment.center,
                    child: _getPayButton(),
                  )
                ],
              )),
        ));
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    numberController.removeListener(_getCardTypeFrmNumber);
    numberController.dispose();
    super.dispose();
  }

  void _getCardTypeFrmNumber() {
    String input = CardUtils.getCleanedNumber(numberController.text);
    CardType cardType = CardUtils.getCardTypeFrmNumber(input);
    setState(() {
      this._paymentCard.type = cardType;
    });
  }

  void _validateInputs() {
    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      setState(() {
        _autoValidate = true; // Start validating on every change.
      });
      _showInSnackBar(fixErrorText);
    } else {
      form.save();
      _stripeTokenDataBloc.executeStripeTokenData(StripeTokenRequest(
          ApiConstants.Token_altValue,
          _paymentCard.number,
          _paymentCard.cvv,
          _paymentCard.month,
          _paymentCard.year,
          'usd'));
    }
  }

  Widget _getPayButton() {
    return CustomRaisedButton(
      title: payText.toUpperCase(),
      width: 200,
      onTap: () {
        _validateInputs();
      },
    );
  }

  void _showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }
}
