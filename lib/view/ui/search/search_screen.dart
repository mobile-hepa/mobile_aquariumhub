import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/lottieLoader.dart';
import 'package:aquariumhub/view/utils/text_view.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:share/share.dart';

TextEditingController searchControlller = TextEditingController();

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  double _width;

  @override
  Widget build(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [_backButton(), _searchField(), _filter()],
              ),
            ),
            _horizontalDivider(),
            _searchResult()
          ],
        ),
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Icon(
          Icons.arrow_back,
          color: blackColor,
        ));
  }

  Widget _searchField() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: TextField(
          controller: searchControlller,
          keyboardType: TextInputType.text,
          decoration: new InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderSide:
                    BorderSide(color: blackColor.withOpacity(0.5), width: 0.0),
                borderRadius: BorderRadius.all(
                  Radius.circular(50.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderRadius: const BorderRadius.all(
                  const Radius.circular(50.0),
                ),
              ),
              filled: true,
              hintStyle: new TextStyle(color: blackColor.withOpacity(0.3)),
              hintText: searchText,
              prefixIcon: Icon(Icons.search),
              contentPadding: EdgeInsets.all(0),
              fillColor: Colors.transparent),
        ),
      ),
    );
  }

  Widget _filter() {
    return GestureDetector(
        onTap: () {
          //Navigator.of(context).pop();
        },
        child: Icon(
          Icons.filter_list,
          color: blackColor,
        ));
  }

  Widget _horizontalDivider() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Container(
        height: 0.3,
        color: blackColor.withOpacity(0.3),
      ),
    );
  }

  Widget _searchResult() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: GridView.builder(
          itemCount: 10,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          physics: AlwaysScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, childAspectRatio: 0.62),
          itemBuilder: (context, index) {
            return _searchCard();
          },
        ),
      ),
    );
  }

  Widget _searchCard() {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, ProductDetailsScreenTag);
      },
      child: Container(
        width: _width / 2,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: _width / 2.4,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8)),
                  child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl:
                          'https://images.unsplash.com/photo-1535591273668-578e31182c4f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM2NTI5fQ',
                      placeholder: (context, url) => lottieLoaderWidget()),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(
                  'by User',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: TextView(
                  'Zoas',
                  textColor: gray600Color,
                  fontSize: 14,
                  maxLines: 1,
                  textOverflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(
                  dollar + "30.00",
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: TextView(
                  sizeText + paddingText + 'S',
                  textColor: gray600Color,
                  fontSize: 14,
                  maxLines: 1,
                  textOverflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {},
                      child: Icon(
                        Icons.add_circle_outline,
                        color: primaryColor,
                        size: 20,
                      ),
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    GestureDetector(
                      onTap: () {
                        Share.share(
                          "https://images.unsplash.com/photo-1535591273668-578e31182c4f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM2NTI5fQ",
                          subject: "Follow Me",
                        );
                      },
                      child: Icon(
                        MdiIcons.shareOutline,
                        color: orangeColor,
                        size: 20,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
