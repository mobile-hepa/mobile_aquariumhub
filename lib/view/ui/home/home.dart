import 'package:aquariumhub/model/beans/HomeVo.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/SizeConfig.dart';
import 'package:aquariumhub/view/utils/lottieLoader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeVo homeVo = HomeVo();

  @override
  void initState() {
    super.initState();

    Data data = new Data();
    data.title = "Title";
    data.items = new List();

    Items item = new Items();
    item.title = "Sed ut perspiciatisunde";
    //item.image = "https://images.unsplash.com/photo-1535591273668-578e31182c4f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM2NTI5fQ";
    item.image = "https://www.thesprucepets.com/thmb/accBJEN78d8nfei7Y5ryGvzFaWw=/1415x1415/smart/filters:no_upscale()/Betta-fish-GettyImages-522586729-58cc1cf15f9b581d72fdb557.jpg";

    data.items.add(item);
    data.items.add(item);
    data.items.add(item);
    data.items.add(item);
    data.items.add(item);
    data.items.add(item);
    data.items.add(item);
    data.items.add(item);
    data.items.add(item);
    data.items.add(item);

    homeVo.data = new List();
    homeVo.data.add(data);
    homeVo.data.add(data);
    homeVo.data.add(data);
    homeVo.data.add(data);
    homeVo.data.add(data);
    homeVo.data.add(data);
    homeVo.data.add(data);
    homeVo.data.add(data);
    homeVo.data.add(data);
    homeVo.data.add(data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: listItem(homeVo.data),
      ),
    );
  }

  Widget listItem(List<Data> data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.all(0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(data[index].title),
              categoryItem(data[index].items)
            ],
          ),
        );
      },
    );
  }

  categoryItem(List<Items> data) {
    return SizedBox(
      height: getProportionateScreenHeight(135),
      width: double.infinity,
      child: ListView.builder(
        itemCount: data.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(
                left: (index == 0) ? getProportionateScreenWidth(10) : getProportionateScreenWidth(5),
                right: (index == data.length-1) ? getProportionateScreenWidth(10) : getProportionateScreenWidth(5)),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: new BorderRadius.all(
                      Radius.circular(
                          getProportionateScreenHeight(12))),
                  gradient: LinearGradient(begin: Alignment.bottomRight, colors: [
                    blackColor,
                    blackColor.withOpacity(0)
                  ])
              ),
              child: CachedNetworkImage(
                  imageUrl: data[index].image,
                  placeholder: (context, url) => lottieLoaderWidget(),
                  imageBuilder: (context, imageProvider) => Container(
                    height: getProportionateScreenHeight(135),
                    width: getProportionateScreenWidth(135),
                    decoration: BoxDecoration(
                      borderRadius: new BorderRadius.all(
                          Radius.circular(
                              getProportionateScreenHeight(12))),
                      image: DecorationImage(
                          image: imageProvider, fit: BoxFit.cover),
                    ),
                  )),
            ),
          );
        },
      ),
    );
  }
}
