import 'package:aquariumhub/bloc/CardDeleteBloc.dart';
import 'package:aquariumhub/bloc/CardListDataBloc.dart';
import 'package:aquariumhub/model/beans/CardList.dart';
import 'package:aquariumhub/model/remote/ApiConstants.dart';
import 'package:aquariumhub/model/remote/requests/CardDeleteRequest.dart';
import 'package:aquariumhub/model/remote/requests/CardListRequest.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/lottieLoader.dart';
import 'package:aquariumhub/view/utils/payment_card.dart';
import 'package:aquariumhub/view/utils/text_view.dart';
import 'package:aquariumhub/view/utils/widgets/custom_dialogue_two_btn.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CardDetails extends StatefulWidget {
  CardDetails();

  @override
  _CardDetailsState createState() => _CardDetailsState();
}

class _CardDetailsState extends State<CardDetails> {
  bool isloading = true;
  List<StripeCards> cardList = new List();
  int cardListPosition = 0;
  bool isDataFound = false;
  CardListDataBloc _cardListDataBloc = CardListDataBloc();
  CardDeleteBloc _cardDeleteBloc = CardDeleteBloc();

  @override
  void initState() {
    // TODO: implement initState
    _cardListDataBloc.executeCardListData(
        CardListRequest(ApiConstants.STRIPE_CONNECT, ApiConstants.getCards));

    _cardListDataBloc.subject.listen((value) {
      if (value.status == Status.SUCCESS) {
        cardList = value.data.Result.data.stripe.stripeCards;
        setState(() {
          isDataFound = true;
          isloading = false;
        });
      }
    });

    _cardDeleteBloc.subject.listen((value) {
      if (value.status == Status.SUCCESS) {
        setState(() {
          Navigator.pop(context);
          _cardListDataBloc.executeCardListData(CardListRequest(
              ApiConstants.STRIPE_CONNECT, ApiConstants.getCards));
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        backgroundColor: primaryColor,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, AddCardPageTag);
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 10, right: 10),
                child: Icon(
                  Icons.add,
                  color: whiteColor,
                  size: 20.0,
                ),
              ),
            ),
          ),
        ],
        title: TextView(
          cardDetailsText,
          textColor: whiteColor,
          fontSize: 20,
        ),
      ),
      body: isloading
          ? lottieLoaderWidget()
          : Stack(children: <Widget>[
        isDataFound
            ? ListView.builder(
          padding: EdgeInsets.all(10),
          shrinkWrap: true,
          itemCount: cardList.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
                child: Card(
                  color: cardList[index].isDefault == "1"
                      ? gray600Color
                      : grayLightColor,
                  elevation: 5,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      //height: 1500.0,
                      child: Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.end,
                              children: <Widget>[
                                CardUtils.getCardIconViaType(
                                    cardList[index].cardType),
                              ],
                            ),
                            Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: [
                                    TextView(cardHintText + " : ",
                                        fontFamily: 'Poppins-SemiBold',
                                        textColor: whiteColor,
                                        fontSize: 14),
                                    TextView(
                                        "XXXX XXXX XXXX " +
                                            cardList[index].last4,
                                        fontFamily: 'Poppins-Regular',
                                        textColor: lightColor,
                                        fontSize: 14),
                                  ],
                                ),
                                Row(
                                  children: [
                                    TextView(expiryHintText + " : ",
                                        fontFamily: 'Poppins-SemiBold',
                                        textColor: whiteColor,
                                        fontSize: 14),
                                    TextView(
                                        cardList[index].expiryMonth +
                                            "/" +
                                            cardList[index].expiryYear,
                                        fontFamily: 'Poppins-Regular',
                                        textColor: whiteColor,
                                        fontSize: 14),
                                  ],
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.end,
                              children: <Widget>[
                                GestureDetector(
                                  child: Icon(
                                            Icons.delete,
                                            color: primaryColor,
                                            size: 20.0,
                                          ),
                                          onTap: () {
                                            showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return CustomDialogueTwoBtn(
                                                    title: appTitle,
                                                    message: deleteAlertText,
                                                    firstBtnName: okBTN,
                                                    secondBtnName: cancelBTN,
                                                    firstBtnClick: () {
                                                      _cardDeleteBloc
                                                          .executeCardDelete(
                                                              CardDeleteRequest(
                                                                  cardList[
                                                                          index]
                                                                      .tokenId));
                                                    },
                                                    secondBtnClick: () {
                                                      Navigator.pop(context);
                                                    },
                                                  );
                                                });
                                          },
                                        ),
                              ],
                            ),
                          ]),
                    ),
                  ),
                ));
          },
        )
            : Center(
          child: TextView(
            noDataFoundText,
            fontSize: 24,
            textColor: appTextColor,
          ),
        ),
      ]),
    );
  }
}
