import 'package:aquariumhub/bloc/GetMyProfileBloc.dart';
import 'package:aquariumhub/model/beans/UserProfile.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/view/ui/account/contact_dialog.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/SizeConfig.dart';
import 'package:aquariumhub/view/utils/lottieLoader.dart';
import 'package:aquariumhub/view/utils/widgets/responsive_ui.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:share/share.dart';

class AccountPage extends StatefulWidget {
  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  double _height;
  double _width;
  double _pixelRatio;
  bool _large;
  bool _medium;
  GetMyProfileBloc _getMyProfileBloc = GetMyProfileBloc();
  UserProfileData userProfileData;
  bool isLoad = true;

  @override
  void initState() {
    // TODO: implement initState
    _getMyProfileBloc.executeGetMyProfile();

    _getMyProfileBloc.subject.listen((value) {
      if (value.status == Status.LOADING) {
        EasyLoading.show(status: 'loading...');
      } else if (value.status == Status.SUCCESS) {
        EasyLoading.dismiss();
        setState(() {
          isLoad = false;
          userProfileData = value.data.Result.data;
        });
      } else if (value.status == Status.ERROR) {
        EasyLoading.dismiss();
        setState(() {
          isLoad = false;
        });
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    _large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    _medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);

    return isLoad
        ? Center()
        : Container(
            child: Column(
              children: [
                banner(),
                SizedBox(
                  height: getProportionateScreenHeight(10),
                ),
                onlineStatus(),
                SizedBox(
                  height: getProportionateScreenHeight(10),
                ),
                edit_contact(),
                SizedBox(
                  height: getProportionateScreenHeight(10),
                ),
                like_follow(),
                SizedBox(
                  height: getProportionateScreenHeight(10),
                ),
                gridViewPosts(),
              ],
            ),
          );
  }

  Widget banner() {
    return SizedBox(
      height: getProportionateScreenHeight(200),
      child: Stack(
        fit: StackFit.expand,
        children: [
          coverImage(),
          whiteShade(),
          profileImage(),
          onlineIcon(),
          emailText(),
          location()
        ],
      ),
    );
  }

  Widget coverImage() {
    return CachedNetworkImage(
      imageUrl:
          userProfileData.bannerUrl != null ? userProfileData.bannerUrl : "",
      placeholder: (context, url) => lottieLoaderWidget(),
      fit: BoxFit.cover,
    );
  }

  Widget whiteShade() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: getProportionateScreenHeight(70),
        color: whiteColor.withOpacity(0.5),
      ),
    );
  }

  Widget profileImage() {
    return Align(
        alignment: Alignment.bottomLeft,
        child: Padding(
            padding: EdgeInsets.all(getProportionateScreenHeight(10)),
            child: Container(
              decoration: new BoxDecoration(
                borderRadius: BorderRadius.all(
                    Radius.circular(getProportionateScreenHeight(50))),
                border: Border.all(color: orangeColor),
              ),
              child: CircleAvatar(
                radius: getProportionateScreenHeight(50),
                backgroundImage: AssetImage(
                  'assets/image/profile.png',
                ),
                backgroundColor: Colors.transparent,
              ),
            )));
  }

  Widget onlineIcon() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Padding(
        padding: EdgeInsets.only(
            left: getProportionateScreenHeight(85),
            bottom: getProportionateScreenHeight(15)),
        child: Icon(
          Icons.circle,
          size: getProportionateScreenHeight(15),
          color: greenColor,
        ),
      ),
    );
  }

  Widget emailText() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Padding(
        padding: EdgeInsets.only(
            left: getProportionateScreenHeight(120),
            bottom: getProportionateScreenHeight(40)),
        child: Text(
          "joseph.meza@gmail.com",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget location() {
    return Align(
      alignment: Alignment.bottomRight,
      child: Padding(
        padding: EdgeInsets.only(
            bottom: getProportionateScreenHeight(10),
            right: getProportionateScreenHeight(10)),
        child: Wrap(children: [
          Container(
            height: getProportionateScreenHeight(25),
            width: getProportionateScreenHeight(105),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(
                    Radius.circular(getProportionateScreenHeight(5))),
                border: Border.all(color: primaryColor),
                color: whiteColor),
            child: Row(
              children: [
                Icon(
                  Icons.location_on,
                  size: getProportionateScreenHeight(20),
                  color: primaryColor,
                ),
                Text(
                  "New York, US",
                  style: TextStyle(
                      color: blackColor,
                      fontSize: getProportionateScreenHeight(11)),
                )
              ],
            ),
          ),
        ]),
      ),
    );
  }

  Widget like_follow() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        infoText(listingText, '0'),
        Container(
          height: getProportionateScreenHeight(20),
          width: getProportionateScreenHeight(1),
          color: primaryColor,
        ),
        infoText(likesText, '0'),
        Container(
          height: getProportionateScreenHeight(20),
          width: getProportionateScreenHeight(1),
          color: primaryColor,
        ),
        infoText(followersText,
            userProfileData.followers != "" ? userProfileData.followers : "0"),
        Container(
          height: getProportionateScreenHeight(20),
          width: getProportionateScreenHeight(1),
          color: primaryColor,
        ),
        infoText(followingText,
            userProfileData.following != "" ? userProfileData.following : "0"),
      ],
    );
  }

  Widget infoText(String title, String val) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        children: [
          TextSpan(
            text: val + '\n',
            style: TextStyle(color: primaryColor, fontSize: 18),
          ),
          TextSpan(
            text: title,
            style: TextStyle(color: blackColor),
          ),
        ],
      ),
    );
  }

  Widget onlineStatus() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              children: [
                TextSpan(
                  text: memberSinceText,
                  style: TextStyle(color: blackColor, fontSize: 14),
                ),
                TextSpan(
                  text: "Jan 2020",
                  style: TextStyle(color: Colors.grey[600], fontSize: 14),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 8,
          ),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              children: [
                TextSpan(
                  text: lastActiveText,
                  style: TextStyle(color: blackColor, fontSize: 14),
                ),
                TextSpan(
                  text: "Online",
                  style: TextStyle(color: Colors.grey[600], fontSize: 14),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget edit_contact() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                  side: BorderSide(color: primaryColor)),
              onPressed: () {},
              color: primaryColor,
              textColor: Colors.white,
              child:
                  Text(editText.toUpperCase(), style: TextStyle(fontSize: 14)),
            ),
          ),
          SizedBox(
            width: 8,
          ),
          Expanded(
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                  side: BorderSide(color: orangeColor)),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return ContactDialog(
                          /*title: "Custom Dialog Demo",
                        descriptions: "Hii all this is a custom dialog in flutter and  you will be use in your flutter applications",
                        text: "Yes",*/
                          );
                    });
              },
              color: orangeColor,
              textColor: Colors.white,
              child: Text(contactText.toUpperCase(),
                  style: TextStyle(fontSize: 14)),
            ),
          ),
        ],
      ),
    );
  }

  Widget gridViewPosts() {
    return Expanded(
      child: GridView.builder(
        itemCount: 4,
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: AlwaysScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 0.55),
        itemBuilder: (context, index) {
          return cardPost();
        },
      ),
    );
  }

  Widget cardPost() {
    return GestureDetector(
      onTap: () {
        // Navigator.pushNamed(context, ProductDetailsScreenTag);
      },
      child: Container(
        width: _width / 2,
        //  height: 300,
        padding: EdgeInsets.only(bottom: 8),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: _width / 2,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8)),
                  child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl:
                          'https://images.unsplash.com/photo-1535591273668-578e31182c4f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM2NTI5fQ',
                      placeholder: (context, url) => lottieLoaderWidget()),
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(
                  'Montipora',
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      'assets/image/profile.png',
                      width: 30,
                      height: 30,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      'Admin',
                      style: TextStyle(color: primaryColor),
                    )
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "\$" + "30",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    Expanded(
                      child: SizedBox(),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Icon(
                        Icons.favorite_border,
                        size: 20,
                      ),
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    GestureDetector(
                      onTap: () {
                        Share.share(
                          "https://images.unsplash.com/photo-1535591273668-578e31182c4f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM2NTI5fQ",
                          subject: "Follow Me",
                        );
                      },
                      child: Icon(
                        Icons.share_sharp,
                        size: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(
                  'Size: S',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w200),
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
