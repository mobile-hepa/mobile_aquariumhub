import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/SizeConfig.dart';
import 'package:aquariumhub/view/utils/lottieLoader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class SigninDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.all(getProportionateScreenHeight(15)),
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: secondaryColor,
          borderRadius: BorderRadius.circular(getProportionateScreenHeight(10)),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Padding(
                      padding: EdgeInsets.all(getProportionateScreenHeight(5)),
                      child: Icon(
                        Icons.cancel,
                        color: whiteColor,
                      ),
                    )),
              ),
              Image.asset(
                'assets/image/logo_light.png',
                height: getProportionateScreenHeight(80),
                width: getProportionateScreenWidth(150),
                fit: BoxFit.fill,
              ),
              SizedBox(
                height: getProportionateScreenHeight(10),
              ),
              Center(
                child: Text(
                  directMessageText,
                  style: TextStyle(
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(10),
              ),
              Row(
                children: [
                  CachedNetworkImage(
                      height: getProportionateScreenHeight(50),
                      width: getProportionateScreenHeight(50),
                      imageUrl:
                          'https://i.insider.com/5dc098e0d8d84605b9674ef9?width=600&format=jpeg&auto=webp',
                      placeholder: (context, url) => lottieLoaderWidget(),
                      imageBuilder: (context, imageProvider) => Container(
                            width: getProportionateScreenHeight(25),
                            height: getProportionateScreenHeight(25),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: imageProvider, fit: BoxFit.cover),
                                border: Border.all(color: orangeColor)),
                          )),
                  SizedBox(
                    width: 15,
                  ),
                  Text(
                    "joseph.meza112@gmail.com",
                    style: TextStyle(color: whiteColor, fontSize: 14),
                  )
                ],
              ),
              SizedBox(
                height: getProportionateScreenHeight(20),
              ),
              TextFormField(
                maxLines: 5,
                minLines: 5,
                keyboardType: TextInputType.multiline,
                decoration: new InputDecoration(
                    fillColor: whiteColor,
                    filled: true,
                    contentPadding:
                        new EdgeInsets.all(getProportionateScreenHeight(10)),
                    border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(
                          getProportionateScreenHeight(5),
                        ),
                        borderSide: new BorderSide(color: orangeColor))),
              ),
              SizedBox(
                height: getProportionateScreenHeight(10),
              ),
              SizedBox(
                width: double.infinity,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenHeight(5)),
                      side: BorderSide(color: orangeColor)),
                  onPressed: () {},
                  color: orangeColor,
                  textColor: Colors.white,
                  child: Text(sendText.toUpperCase(),
                      style: TextStyle(fontSize: 14)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
