import 'package:aquariumhub/view/ui/dashboard/dashboard_screen.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/validator.dart';
import 'package:aquariumhub/view/utils/widgets/custom_button.dart';
import 'package:aquariumhub/view/utils/widgets/custom_shape.dart';
import 'package:aquariumhub/view/utils/widgets/responsive_ui.dart';
import 'package:aquariumhub/view/utils/widgets/snackbar.dart';
import 'package:aquariumhub/view/utils/widgets/textformfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

enum RadioButtons { seller, buyer }

class _SignUpScreenState extends State<SignUpScreen> {
  bool checkBoxValue = false;
  double _height;
  double _width;
  double _pixelRatio;
  bool _large;
  bool _medium;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey();
  RadioButtons type = RadioButtons.buyer;
  int id = 1;

  NavigatePage(BuildContext context) {
    if (validateEmail(emailController.text) != null) {
      showSnackBar(context, validateEmail(emailController.text));
    } else if (validatePasswordLength(passwordController.text) != null) {
      showSnackBar(context, validatePasswordLength(passwordController.text));
    } else {
      Navigator.pushNamed(context, DashboardTag);
    }
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    _large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    _medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);

    return Material(
      child: Scaffold(
        body: Container(
          height: _height,
          width: _width,
          margin: EdgeInsets.only(bottom: 1),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                clipShape(),
                signUpTextRow(),
                acceptTermsTextRow(),
                form(),
                SizedBox(
                  height: _height / 30,
                ),
                button(),
                infoTextRow(),
                socialIconsRow(),
                signInTextRow(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget clipShape() {
    //double height = MediaQuery.of(context).size.height;
    return Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.75,
          child: ClipPath(
            clipper: CustomShapeClipper(),
            child: Container(
              height: _large
                  ? _height / 4
                  : (_medium ? _height / 3.75 : _height / 3.5),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [primaryColor, secondaryColor],
                ),
              ),
            ),
          ),
        ),
        Opacity(
          opacity: 0.5,
          child: ClipPath(
            clipper: CustomShapeClipper2(),
            child: Container(
              height: _large
                  ? _height / 4.5
                  : (_medium ? _height / 4.25 : _height / 4),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [primaryColor, secondaryColor],
                ),
              ),
            ),
          ),
        ),
        Container(
          alignment: Alignment.bottomCenter,
          margin: EdgeInsets.only(
              top: _large
                  ? _height / 30
                  : (_medium ? _height / 25 : _height / 20)),
          child: Image.asset(
            'assets/image/login.png',
            height: _height / 3.5,
            width: _width / 3.5,
          ),
        ),
      ],
    );
  }

  Widget form() {
    return Container(
      margin: EdgeInsets.only(
          left: _width / 12.0, right: _width / 12.0, top: _height / 45.0),
      child: Form(
        key: _key,
        child: Column(
          children: <Widget>[
            emailTextFormField(),
            SizedBox(height: _height / 60.0),
            passwordTextFormField(),
          ],
        ),
      ),
    );
  }

  Widget signUpTextRow() {
    return Container(
      margin: EdgeInsets.only(left: _width / 10.0, right: _width / 10.0),
      child: Row(
        children: <Widget>[
          Flexible(
            child: Text(
              signUpAccountText,
              maxLines: 3,
              style: TextStyle(
                fontWeight: FontWeight.w200,
                fontSize: _large ? 20 : (_medium ? 17.5 : 15),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget emailTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.emailAddress,
      textEditingController: emailController,
      icon: Icons.email,
      hint: EmailIDText,
    );
  }

  Widget passwordTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.visiblePassword,
      obscureText: true,
      icon: Icons.lock,
      textEditingController: passwordController,
      hint: PasswordText,
      isPassword: true,
      action: TextInputAction.done,
    );
  }

  Widget acceptTermsTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 150.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Radio(
            value: RadioButtons.seller,
            groupValue: type,
            onChanged: (RadioButtons value) {
              setState(() {
                type = value;
              });
            },
          ),
          Text(
            imSellerText,
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: _large ? 12 : (_medium ? 11 : 10)),
          ),
          Radio(
            value: RadioButtons.buyer,
            groupValue: type,
            onChanged: (RadioButtons value) {
              setState(() {
                type = value;
              });
            },
          ),
          Text(
            imBuyerText,
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: _large ? 12 : (_medium ? 11 : 10)),
          ),
        ],
      ),
    );
  }

  Widget button() {
    return ChangeNotifierProvider(
      create: (_) => MenuProvider(),
      builder: (context, child) => CustomRaisedButton(
        width: _large ? _width / 4 : (_medium ? _width / 3.75 : _width / 3.5),
        fontSize: _large ? 14 : (_medium ? 12 : 10),
        title: signUpText,
        onTap: () {
          NavigatePage(context);
        },
      ),
    );
  }

  Widget infoTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 40.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            usingSocialMediaText,
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: _large ? 12 : (_medium ? 11 : 10)),
          ),
        ],
      ),
    );
  }

  Widget socialIconsRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 80.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              signInWithGoogle();
            },
            child: CircleAvatar(
              radius: 16,
              backgroundImage: AssetImage("assets/image/googlelogo.png"),
            ),
          ),
          SizedBox(
            width: 20,
          ),
          GestureDetector(
            onTap: () {signInWithFacebook();},
            child: CircleAvatar(
              radius: 15,
              backgroundImage: AssetImage("assets/image/fblogo.jpg"),
            ),
          ),
        ],
      ),
    );
  }

  Widget signInTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            alreadyHaveAccountText,
            style: TextStyle(fontWeight: FontWeight.w400),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).pop(SignInScreenPageTag);
            },
            child: Text(
              signInText,
              style: TextStyle(
                  fontWeight: FontWeight.w800,
                  color: primaryColor,
                  fontSize: 19),
            ),
          )
        ],
      ),
    );
  }

  Future<void> signInWithGoogle() async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount googleSignInAccount =
    await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final UserCredential authResult =
    await _auth.signInWithCredential(credential);
    try{
      final User user = authResult.user;

      if (user != null) {
        assert(!user.isAnonymous);
        assert(await user.getIdToken() != null);

        final User currentUser = _auth.currentUser;
        assert(user.uid == currentUser.uid);

        print('signInWithGoogle succeeded: $user');
        //return '$user';
      } else {
        print('no user exists, please sign up');
      }
    } catch(e) {
      print("Error =>" + e.toString());
    }

  }

  Future<void> signInWithFacebook() async {
    try {
      // Trigger the sign-in flow
      final AccessToken result = await FacebookAuth.instance.login();

      // Create a credential from the access token
      final FacebookAuthCredential facebookAuthCredential =
      FacebookAuthProvider.credential(result.token);

      UserCredential credential = await FirebaseAuth.instance
          .signInWithCredential(facebookAuthCredential);
      // Once signed in, return the UserCredential
      print(credential);
      final User user = credential.user;

      //user.uid
      //user.name

      if (user != null) {
        print(user.displayName);
      }
    } on FacebookAuthException catch (e) {
      switch (e.errorCode) {
        case FacebookAuthErrorCode.OPERATION_IN_PROGRESS:
          print("You have a previous login operation in progress");
          break;
        case FacebookAuthErrorCode.CANCELLED:
          print("login cancelled");
          break;
        case FacebookAuthErrorCode.FAILED:
          print("login failed");
          break;
      }
    }
  }
}
