import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/SizeConfig.dart';
import 'package:aquariumhub/view/utils/lottieLoader.dart';
import 'package:aquariumhub/view/utils/widgets/post_button.dart';
import 'package:aquariumhub/view/utils/widgets/profile_avatar.dart';
import 'package:aquariumhub/view/utils/widgets/responsive_ui.dart';
import 'package:aquariumhub/view/utils/widgets/title_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:share/share.dart';

class ScrollFeed extends StatefulWidget {
  @override
  _ScrollFeedState createState() => _ScrollFeedState();
}

class _ScrollFeedState extends State<ScrollFeed> {
  double _height;
  double _width;
  double _pixelRatio;
  bool _large;
  bool _medium;
  TextEditingController searchControlller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    _large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    _medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);

    SizeConfig().init(context);
    return Container(
      width: _width,
      height: _height,
      padding: EdgeInsets.all(10),
      child: Builder(
        builder: (context) => Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            searchField(),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(top: 5),
                child: ListView.builder(
                  itemCount: 5,
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  physics: AlwaysScrollableScrollPhysics(),
                  // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  //     crossAxisCount: 1),
                  itemBuilder: (context, index) {
                    return cardFeed();
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget searchField() {
    return GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed(SearchTag);
        },
        child:
            /*CustomTextField(
        keyboardType: TextInputType.text,
        hint: 'Search',
        icon: Icons.search,
        textEditingController: searchControlller,
        borderRadius: 15,
      ),*/
            Text("Search"));
  }

  Widget cardFeed() {
    return GestureDetector(
      onTap: () {
        // Navigator.pushNamed(context, ProductDetailsScreenTag);
      },
      child: Container(
        width: _width,
        //height: _height,
        padding: EdgeInsets.only(top: 5),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(16)),
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              feedHeader(),
              feedPostImage(),
              productNameAndPrice(),
              _availableSize(),
              likeCommentShare()
            ],
          ),
        ),
      ),
    );
  }

  Widget feedHeader() {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        children: [
          ProfileAvatar(
              imageUrl:
              'https://randomuser.me/api/portraits/women/49.jpg'),
          const SizedBox(width: 8.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'username',
                  style: const TextStyle(
                      //fontWeight: FontWeight.w500,
                      color: primaryColor,
                      fontSize: 16),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  'Posted this on Tuesday',
                  style: TextStyle(
                      //fontWeight: FontWeight.w500,
                      color: blackColor.withOpacity(0.8),
                      fontSize: 13),
                ),
                SizedBox(
                  height: 2,
                ),
                Row(
                  children: [
                    Text(
                      'timeAgo • ',
                      style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 12.0,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }


  Widget feedPostImage() {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, ProductDetailsScreenTag);
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Center(
            child: CachedNetworkImage(
          height: getProportionateScreenHeight(300),
          width: double.infinity,
          fit: BoxFit.cover,
          imageUrl:
              'https://images.unsplash.com/photo-1535591273668-578e31182c4f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM2NTI5fQ',
          placeholder: (context, url) => lottieLoaderWidget(),
        )),
      ),
    );
  }

  Widget _availableSize() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                _priceWidget(dollar + '15.00'),
                _verticalDivider(),
                _sizeWidget(sizeText + ' L'),
                _verticalDivider(),
                _sizeWidget('Zoanthid'),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _verticalDivider(
      {Color color = orangeColor, double horizontalPadding = 20.0}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
      child: Container(
        width: 2,
        height: 20,
        color: color,
      ),
    );
  }

  Widget _priceWidget(String text,
      {Color color = yellowColor, bool isSelected = false}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: Text(
        text,
        style: TextStyle(
            color: blackColor, fontSize: 16, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _sizeWidget(String text,
      {Color color = yellowColor, bool isSelected = false}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      // decoration: BoxDecoration(
      //   border: Border.all(color: yellowColor, style: BorderStyle.none),
      //   borderRadius: BorderRadius.all(Radius.circular(13)),
      //   color: Theme.of(context).backgroundColor,
      // ),
      child: TitleText(
        text: text,
        fontSize: 14,
        color: primaryColor,
      ),
    );
  }

  _widgetWithColor(Color color) {
    return Container(
        padding: EdgeInsets.symmetric(
          vertical: 5,
        ),
        // decoration: BoxDecoration(
        //   border: Border.all(color: yellowColor, style: BorderStyle.none),
        //   borderRadius: BorderRadius.all(Radius.circular(13)),
        //   color: Theme.of(context).backgroundColor,
        // ),
        child: Row(
          children: [
            TitleText(
              text: colorText + paddingText,
              fontSize: 14,
            ),
            _colorWidget(color),
          ],
        ));
  }

  Widget _colorWidget(Color color, {bool isSelected = false}) {
    return CircleAvatar(
      radius: 10,
      backgroundColor: color.withAlpha(150),
      child: isSelected
          ? Icon(
              Icons.check_circle,
              color: color,
              size: 14,
            )
          : CircleAvatar(radius: 7, backgroundColor: color),
    );
  }

  Widget productNameAndPrice() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: TitleText(text: "Zoas", fontSize: 18),
    );
  }

  Widget likeCommentShare() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                padding: const EdgeInsets.all(4.0),
                decoration: BoxDecoration(
                  color: primaryColor,
                  shape: BoxShape.circle,
                ),
                child: const Icon(
                  Icons.thumb_up,
                  size: 10.0,
                  color: Colors.white,
                ),
              ),
              const SizedBox(width: 4.0),
              Expanded(
                child: Text(
                  '5 likes',
                  style: TextStyle(
                    color: Colors.grey[600],
                  ),
                ),
              ),
              Text(
                '12 Comments',
                style: TextStyle(
                  color: Colors.grey[600],
                ),
              ),
            ],
          ),
          const Divider(),
          Row(
            children: [
              PostButton(
                icon: Icon(
                  MdiIcons.heartOutline,
                  color: redColor,
                  size: 20.0,
                ),
                label: likeText,
                onTap: () => print('Like'),
              ),
              PostButton(
                icon: Icon(
                  MdiIcons.commentOutline,
                  color: Colors.grey[600],
                  size: 20.0,
                ),
                label: commentText,
                onTap: () => print('Comment'),
              ),
              PostButton(
                  icon: Icon(
                    MdiIcons.shareOutline,
                    color: Colors.grey[600],
                    size: 20.0,
                  ),
                  label: shareText,
                  onTap: () {
                    Share.share(
                      "https://suryadevsingh24032000.medium.com/",
                      subject: "Follow Me",
                    );
                  })
            ],
          ),
        ],
      ),
    );
  }
}
