import 'package:aquariumhub/view/ui/dashboard/dashboard_screen.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/lottieLoader.dart';
import 'package:aquariumhub/view/utils/text_view.dart';
import 'package:aquariumhub/view/utils/widgets/custom_button.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ShoppingCartScreen extends StatefulWidget {
  @override
  _ShoppingCartScreenState createState() => _ShoppingCartScreenState();
}

class _ShoppingCartScreenState extends State<ShoppingCartScreen> {
  double _height;
  double _width;
  int count = 0;

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: TextView(
          yourCartText,
          textColor: whiteColor,
          fontSize: 20,
        ),
      ),
      body: Container(
        color: backgroundColor,
        height: _height,
        width: _width,
        child: Center(
          child: Column(
            children: [
              /*lottieEmptyWidget(),
              welcomeTextRow(),
              signInTextRow(),*/
              createCartList(),
              ShoppingCartTotal(),
            ],
          ),
        ),
      ),
    );
  }

  Widget welcomeTextRow() {
    return Container(
      margin: EdgeInsets.only(left: _width / 15.0, right: 15.0),
      child: Row(
        children: <Widget>[
          Text(
            cartEmptyText,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }

  Widget signInTextRow() {
    return Container(
      margin: EdgeInsets.only(left: _width / 15.0, right: 15.0),
      child: Row(
        children: <Widget>[
          Flexible(
            child: Text(
              noItemAddedText,
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontWeight: FontWeight.w200,
                fontSize: 13,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget createCartList() {
    return Expanded(
      child: ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemBuilder: (context, position) {
          return createCartListItem();
        },
        itemCount: 5,
      ),
    );
  }

  Widget createCartListItem() {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 8, left: 8, top: 8, bottom: 8),
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(14)),
                  color: Colors.blue.shade200,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl:
                          'https://images.unsplash.com/photo-1535591273668-578e31182c4f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM2NTI5fQ',
                      placeholder: (context, url) => lottieLoaderWidget()),
                ),
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(right: 8, top: 4),
                        child: Text(
                          "NIKE XTM Basketball Shoeas",
                          maxLines: 2,
                          softWrap: true,
                        ),
                      ),
                      SizedBox(height: 6),
                      Text(
                        "Green M",
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "\$299.00",
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: _itemCount(),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                flex: 100,
              )
            ],
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Container(
            width: 24,
            height: 24,
            alignment: Alignment.center,
            margin: EdgeInsets.only(right: 10, top: 8),
            child: Icon(
              Icons.close,
              color: Colors.white,
              size: 20,
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(4)),
                color: Colors.green),
          ),
        )
      ],
    );
  }

  Widget _itemCount() {
    return Align(
      alignment: Alignment.centerRight,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(
            height: 40,
            width: 40,
            child: RaisedButton(
              child: Text(
                "-",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              onPressed: () {
                if (count != 0) {
                  count--;
                  setState(() {});
                }
              },
              color: whiteColor,
              textColor: blackColor,
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              splashColor: Colors.grey,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Text(count.toString()),
          ),
          SizedBox(
            height: 40,
            width: 40,
            child: RaisedButton(
              child: Text(
                "+",
                style: TextStyle(fontSize: 16),
              ),
              onPressed: () {
                count++;
                setState(() {});
              },
              color: whiteColor,
              textColor: blackColor,
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              splashColor: Colors.grey,
            ),
          )
        ],
      ),
    );
  }

  Widget ShoppingCartTotal() {
    return Container(
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.only(
          top: 14,
        ),
        decoration: BoxDecoration(color: Colors.white),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(cartTotalText,
                    style: TextStyle(fontSize: 16, color: primaryColor))
              ],
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(subTotalText, style: TextStyle(fontSize: 18)),
                  Text("\$0.0", style: TextStyle(fontSize: 14))
                ]),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(shippingText, style: TextStyle(fontSize: 18)),
                  Text("\$0.0", style: TextStyle(fontSize: 14))
                ]),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(grandTotalText, style: TextStyle(fontSize: 18)),
                  Text("\$0.0", style: TextStyle(fontSize: 14))
                ]),
            Container(
                width: MediaQuery.of(context).size.width,
                child: Center(child: button()))
          ],
        ));
  }

  Widget button() {
    return ChangeNotifierProvider(
        create: (_) => MenuProvider(),
        builder: (context, child) => CustomRaisedButton(
              title: proceedToCheckoutBTN,
              width: MediaQuery.of(context).size.width,
              fontSize: 14,
              onTap: () {},
            ));
  }
}
