import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/SizeConfig.dart';
import 'package:aquariumhub/view/utils/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

TextEditingController controller = TextEditingController(text: "");

class OtpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


    return Scaffold(
      backgroundColor: darkBlueColor,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: getProportionateScreenHeight(16),
              horizontal: getProportionateScreenWidth(12)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              backButton(context),
              SizedBox(height: getProportionateScreenHeight(50),),
              enterText(),
              SizedBox(height: getProportionateScreenHeight(11),),
              pinView(),
              SizedBox(height: getProportionateScreenHeight(16),),
              didntText(),
              SizedBox(height: getProportionateScreenHeight(32)),
              continueButton(context),
              SizedBox(height: getProportionateScreenHeight(15),),
              fbText(),
            ],
          ),
        ),
      ),
    );
  }

  Widget backButton(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Icon(
            Icons.arrow_back,
            color: whiteColor,
            size: getProportionateScreenHeight(24),
          ),
        ),
      ),
    );
  }

  Widget enterText() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(44)),
      child: Text(
        enterOtpText,
        textAlign: TextAlign.center,
        style: TextStyle(
            color: whiteColor, fontSize: 18, fontFamily: 'Inter-bold'),
      ),
    );
  }

  Widget pinView() {
    return PinCodeTextField(
      autofocus: false,
      controller: controller,
      maxLength: 4,
      onDone: (text) {
        print("DONE $text");
        print("DONE CONTROLLER ${controller.text}");
      },
      pinBoxWidth:  getProportionateScreenWidth(38),
      pinBoxHeight:  getProportionateScreenHeight(48),
      wrapAlignment: WrapAlignment.center,
      pinTextStyle: TextStyle(fontSize: 22.0),
      keyboardType: TextInputType.number,
      pinBoxRadius: getProportionateScreenHeight(6),
      pinBoxColor: whiteColor,
      defaultBorderColor: whiteColor,
      highlightPinBoxColor: whiteColor,
      hasTextBorderColor: whiteColor,
      pinBoxOuterPadding: EdgeInsets.all(5),
    );
  }

  Widget didntText() {
    return Text(
      didntGetOtpText,
      textAlign: TextAlign.center,
      style: TextStyle(
          color: whiteColor.withOpacity(0.8),
          fontSize: 12,
          fontFamily: 'Inter-Regular',
          decoration: TextDecoration.underline),
    );
  }

  Widget continueButton(BuildContext context) {
    return CustomRaisedButton(
      title: continueBTN,
      width: getProportionateScreenWidth(216),
      height: getProportionateScreenHeight(47),
      onTap: () {
        Navigator.of(context).pushNamed(DashboardTag);
      },
      fontSize: 14,
      bgColor: whiteColor,
      textColor: btnTextBlack,
    );
  }

  Widget fbText() {
    return Text(
      useFbText,
      textAlign: TextAlign.center,
      style: TextStyle(
          color: whiteColor,
          fontSize: 12,
          fontFamily: 'Inter-Regular'),
    );
  }
}
