import 'package:aquariumhub/bloc/SignInDataBloc.dart';
import 'package:aquariumhub/main.dart';
import 'package:aquariumhub/model/remote/requests/SignInRequest.dart';
import 'package:aquariumhub/model/remote/util/ApiResponse.dart';
import 'package:aquariumhub/view/ui/dashboard/dashboard_screen.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/shared_data.dart';
import 'package:aquariumhub/view/utils/validator.dart';
import 'package:aquariumhub/view/utils/widgets/custom_button.dart';
import 'package:aquariumhub/view/utils/widgets/custom_shape.dart';
import 'package:aquariumhub/view/utils/widgets/responsive_ui.dart';
import 'package:aquariumhub/view/utils/widgets/snackbar.dart';
import 'package:aquariumhub/view/utils/widgets/textformfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  double _height;
  double _width;
  double _pixelRatio;
  bool _large;
  bool _medium;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey();
  SignInDataBloc _signInDataBloc = SignInDataBloc();

  NavigatePage(BuildContext context) {
    if (validateEmail(emailController.text) != null) {
      showSnackBar(context, validateEmail(emailController.text));
    } else if (validatePasswordLength(passwordController.text) != null) {
      showSnackBar(context, validatePasswordLength(passwordController.text));
    } else {
      _signInDataBloc.executeSignInData(
          SignInRequest(emailController.text, passwordController.text, "en"));
      //Navigator.pushNamed(context, DashboardTag);
    }
  }

  NavigateForGotPassScreen() {
    Navigator.pushNamed(context, ForgotPasswordscreenTag);
  }

  @override
  void initState() {
    // TODO: implement initState
    _signInDataBloc.subject.listen((value) {
      if (value.status == Status.LOADING) {
        EasyLoading.show(status: 'loading...');
      } else if (value.status == Status.SUCCESS) {
        setState(() {
          try {
            getAuthHeaders = getCookieData(value.cookieData);
            SharedData.saveCookieferences(getAuthHeaders);
            isLogin = true;
            SharedData.isUserLoggedIn(true);
            EasyLoading.dismiss();
            Navigator.of(context).pushNamedAndRemoveUntil(
                DashboardTag, (Route<dynamic> route) => false);
          } catch (e) {
            SharedData.isUserLoggedIn(false);
            EasyLoading.showError('Invalid credentials');
            //showSnackBar(context, "Invalid credentials");
            Future.delayed(const Duration(milliseconds: 1000), () {
              EasyLoading.dismiss();
            });
          }
        });
      } else if (value.status == Status.ERROR) {}
    });

    super.initState();
  }

  dynamic getCookieData(String rawCookie) {
    List<String> keyValueCookies = rawCookie.split("; ");
    // NOTE: We are looking for the "alg-wc-wl-user-id", "tk_ai" and "wordpress_sec_e84d3f263737d4bdc70b676b0f704439" keys
    String wcId = "";
    String tkAi = "";
    String wordpressSec = "";
    keyValueCookies.forEach((String cookie) {
      if (cookie.contains("alg-wc-wl-user-id") && wcId == "") {
        wcId = cookie.split("=")[1];
        print("wcId " + wcId);
      } else if (cookie.contains("tk_ai") && tkAi == "") {
        tkAi = cookie.split("=")[1];
        print("tkAi " + tkAi);
      } else if (cookie
              .contains("wordpress_sec_e84d3f263737d4bdc70b676b0f704439") &&
          wordpressSec == "") {
        wordpressSec = cookie.split("=")[2];
        print("wordpressSec " + wordpressSec);
      }
    });

    var dataString =
        "alg-wc-wl-user-id=$wcId;tk_ai=$tkAi;wordpress_sec_e84d3f263737d4bdc70b676b0f704439=$wordpressSec;";
    return dataString;
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    _large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    _medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);
    return Scaffold(
      body: Container(
        height: _height,
        width: _width,
        padding: EdgeInsets.only(bottom: 5),
        child: SingleChildScrollView(
          child: Builder(
            builder: (context) => Column(
              children: <Widget>[
                clipShape(),
                welcomeTextRow(),
                signInTextRow(),
                form(),
                infoTextRow(),
                socialIconsRow(),
                forgetPassTextRow(),
                SizedBox(height: _height / 12),
                button(),
                signUpTextRow(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget clipShape() {
    //double height = MediaQuery.of(context).size.height;
    return Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.75,
          child: ClipPath(
            clipper: CustomShapeClipper(),
            child: Container(
              height: _large
                  ? _height / 4
                  : (_medium ? _height / 3.75 : _height / 3.5),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [primaryColor, secondaryColor],
                ),
              ),
            ),
          ),
        ),
        Opacity(
          opacity: 0.5,
          child: ClipPath(
            clipper: CustomShapeClipper2(),
            child: Container(
              height: _large
                  ? _height / 4.5
                  : (_medium ? _height / 4.25 : _height / 4),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [primaryColor, secondaryColor],
                ),
              ),
            ),
          ),
        ),
        Container(
          alignment: Alignment.bottomCenter,
          margin: EdgeInsets.only(
              top: _large
                  ? _height / 30
                  : (_medium ? _height / 25 : _height / 20)),
          child: Image.asset(
            'assets/image/login.png',
            height: _height / 3.5,
            width: _width / 3.5,
          ),
        ),
      ],
    );
  }

  Widget welcomeTextRow() {
    return Container(
      margin: EdgeInsets.only(left: _width / 15.0, right: 15.0),
      child: Row(
        children: <Widget>[
          Text(
            welcomeText,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }

  Widget signInTextRow() {
    return Container(
      margin: EdgeInsets.only(left: _width / 15.0, right: 15.0),
      child: Row(
        children: <Widget>[
          Flexible(
            child: Text(
              signInAccountText,
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontWeight: FontWeight.w200,
                fontSize: 13,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget form() {
    return Container(
      margin: EdgeInsets.only(
          left: _width / 12.0, right: _width / 12.0, top: _height / 15.0),
      child: Form(
        key: _key,
        child: Column(
          children: <Widget>[
            emailTextFormField(),
            SizedBox(height: _height / 40.0),
            passwordTextFormField(),
          ],
        ),
      ),
    );
  }

  Widget emailTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.emailAddress,
      textEditingController: emailController,
      icon: Icons.email,
      hint: EmailIDText,
    );
  }

  Widget passwordTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.visiblePassword,
      textEditingController: passwordController,
      icon: Icons.lock,
      obscureText: true,
      hint: PasswordText,
      isPassword: true,
      action: TextInputAction.done,
    );
  }

  Widget forgetPassTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 40.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            forgotYourPasswordText,
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: _large ? 14 : (_medium ? 12 : 10)),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: () {
              NavigateForGotPassScreen();
            },
            child: Text(
              recoverText,
              style:
                  TextStyle(fontWeight: FontWeight.w600, color: primaryColor),
            ),
          )
        ],
      ),
    );
  }

  Widget button() {
    return ChangeNotifierProvider(
        create: (_) => MenuProvider(),
        builder: (context, child) => CustomRaisedButton(
              title: signInText,
              width: _large
                  ? _width / 4
                  : (_medium ? _width / 3.75 : _width / 3.5),
              fontSize: _large ? 14 : (_medium ? 12 : 10),
              onTap: () {
                NavigatePage(context);
              },
            ));
  }

  Widget signUpTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 120.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            haveAccountText,
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: _large ? 14 : (_medium ? 12 : 10)),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed(SignUpScreenTag);
            },
            child: Text(
              signUpText,
              style: TextStyle(
                  fontWeight: FontWeight.w800,
                  color: primaryColor,
                  fontSize: _large ? 19 : (_medium ? 17 : 15)),
            ),
          )
        ],
      ),
    );
  }

  Widget infoTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 40.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            usingSocialMediaText,
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: _large ? 12 : (_medium ? 11 : 10)),
          ),
        ],
      ),
    );
  }

  Widget socialIconsRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 80.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              signInWithGoogle();
            },
            child: CircleAvatar(
              radius: 16,
              backgroundImage: AssetImage("assets/image/googlelogo.png"),
            ),
          ),
          SizedBox(
            width: 20,
          ),
          GestureDetector(
            onTap: () {
              signInWithFacebook();
            },
            child: CircleAvatar(
              radius: 15,
              backgroundImage: AssetImage("assets/image/fblogo.jpg"),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> signInWithGoogle() async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final UserCredential authResult =
        await _auth.signInWithCredential(credential);
    try {
      final User user = authResult.user;

      if (user != null) {
        assert(!user.isAnonymous);
        assert(await user.getIdToken() != null);

        final User currentUser = _auth.currentUser;
        assert(user.uid == currentUser.uid);

        print('signInWithGoogle succeeded: $user');
        //return '$user';
      } else {
        print('no user exists, please sign up');
      }
    } catch (e) {
      print("Error =>" + e.toString());
    }
  }

  Future<void> signInWithFacebook() async {
    try {
      // Trigger the sign-in flow
      final AccessToken result = await FacebookAuth.instance.login();

      // Create a credential from the access token
      final FacebookAuthCredential facebookAuthCredential =
          FacebookAuthProvider.credential(result.token);

      UserCredential credential = await FirebaseAuth.instance
          .signInWithCredential(facebookAuthCredential);
      // Once signed in, return the UserCredential
      print(credential);
      final User user = credential.user;

      //user.uid
      //user.name

      if (user != null) {
        print(user.displayName);
      }
    } on FacebookAuthException catch (e) {
      switch (e.errorCode) {
        case FacebookAuthErrorCode.OPERATION_IN_PROGRESS:
          print("You have a previous login operation in progress");
          break;
        case FacebookAuthErrorCode.CANCELLED:
          print("login cancelled");
          break;
        case FacebookAuthErrorCode.FAILED:
          print("login failed");
          break;
      }
    }
  }
}
