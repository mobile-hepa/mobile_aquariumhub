import 'package:aquariumhub/view/ui/dashboard/dashboard_screen.dart';
import 'package:aquariumhub/view/utils/Constants.dart';
import 'package:aquariumhub/view/utils/routes.dart';
import 'package:aquariumhub/view/utils/shared_data.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

bool isLogin;
String getAuthHeaders;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  isLogin = await SharedData.readUserLoggedIn();
  getAuthHeaders = await SharedData.readCookiePreferences();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => MenuProvider())],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Login",
        //theme: themeData(),
        routes: routes,
        initialRoute: SplashScreenPageTag,
        builder: EasyLoading.init(),
      ),
    );
  }
}
